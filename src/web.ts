import type {SeekNowPhotoPickerOptions} from "./definitions";

// export class SeekNowMultipurposeWeb
//   extends WebPlugin
//   implements SeekNowMultipurposePlugin {
//   async echo(options: { value: string }): Promise<{ value: string }> {
//     console.log('ECHO', options);
//     return options;
//   }
//   async openMap(location: OpenMapOptions): Promise<void> {
//     console.log('OpenMap', location);
//   }
//
//     async checkPermissions(): Promise<PermissionStatus> {
//         if (typeof navigator === 'undefined' || !navigator.permissions) {
//             throw this.unavailable('Permissions API not available in this browser.');
//         }
//   }
//
//     async requestPermissions(): Promise<PermissionStatus> {
//        throw this.unimplemented('Not implemented on web.');
//   }
// }
//
// declare const SeekNowPlugin: SeekNowMultipurposeWeb;
// export { SeekNowPlugin }


export class SeekNowPhotoPicker {

    async getPhoto(options: SeekNowPhotoPickerOptions): Promise<any> {
        console.log('*** PETER: Inside SeekNowPhotoPicker.getPhoto');

        const args = [options.sourceType, options.quality, options.encodingType, options.targetWidth, options.targetHeight];

        return new Promise((resolve, reject) => {
            const plugins = cordova.plugins as any;
            console.log('*** PETER: Inside the promise for camera/photo picker');
            plugins.CordovaPluginSeekNowPhotoPicker.getPhoto(resolve, reject, args);
        });
    }

    async pickPhoto(options: SeekNowPhotoPickerOptions): Promise<any> {
        console.log('*** PETER: Inside SeekNowPhotoPicker.pickPhoto');

        const args = [options.sourceType, options.quality, options.encodingType, options.targetWidth, options.targetHeight];

        return new Promise((resolve, reject) => {
            const plugins = cordova.plugins as any;
            console.log('*** PETER: Inside the promise for camera/photo picker');
            plugins.CordovaPluginSeekNowPhotoPicker.pickPhoto(resolve, reject, args);
        });
    }

    getGPSData(path): Promise<any> {
        console.log('*** PETER: Inside getGPSData');

        return new Promise( (resolve, reject) => {
            const plugins = cordova.plugins as any;
            plugins.CordovaPluginSeekNowPhotoPicker.getGPSData(resolve, reject, [path]);
        });
    }

    private initDefaultCameraOptions(): CameraOptions {
        // Backwards compatibility defaults
        const scaleImage = true;
        const targetSize = 640;

        const options: CameraOptions = {
            quality: 50,
            correctOrientation: true // Possibly needed for Android
            // sourceType: 2 // photo library = 0, camera = 1, saved photo album = 2
            // encodingType: camera.EncodingType.JPEG // JPEG is much smaller
        };

        if (scaleImage) {
            options.targetWidth = targetSize; // 1280 -> ~100 KB, 640 -> ~30 KB
            options.targetHeight = targetSize;
        }

        return options;
    }
}
