/*
 * SeekNowMultiPlugin Interfaces
 */
declare module '@capacitor/core' {
  interface PluginRegistry {
    SeekNowMultiPlugin: SeekNowPhotoPicker;
  }
}
export enum PhotoPickerSourceType {
  CAMERA = 'camera',
  LIBRARY = 'library'
}

export enum PhotoPickerEncodingType {
  JPEG = 'jpeg',
  PNG = 'png'
}


export interface SeekNowPhotoPickerOptions {
  quality: number;
  sourceType?: PhotoPickerSourceType;
  encodingType?: PhotoPickerEncodingType;
  targetWidth?: number;
  targetHeight?: number;
}

export interface SeekNowMultiPlugin {
  getPhoto(options: SeekNowPhotoPickerOptions): Promise<any>;
  pickPhoto(options: SeekNowPhotoPickerOptions): Promise<any>;
}
