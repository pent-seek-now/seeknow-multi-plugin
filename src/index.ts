import { registerPlugin } from '@capacitor/core';

import type { SeekNowMultipurposePlugin } from './definitions';

const SeekNowMultipurpose = registerPlugin<SeekNowMultipurposePlugin>(
  'SeekNowMultipurpose',
  {
    web: () => import('./web').then(m => new m.SeekNowMultipurposeWeb()),
  },
);

export * from './definitions';
export { SeekNowMultipurpose };
