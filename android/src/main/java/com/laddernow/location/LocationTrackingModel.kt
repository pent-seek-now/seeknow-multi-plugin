package com.laddernow.location

import android.location.Location
import androidx.lifecycle.MutableLiveData
import kotlin.math.abs

object LocationTrackingModel {

    //
    // Constants
    //
    private const val TAG = "LocationTrackingModel"
    const val distanceThresholdMeters: Float = 10F

    //
    // LiveData
    //
    var hasServiceStarted: Boolean = false
    var isLocationAvailable = MutableLiveData(false)
    var isBackgroundTrackingEnabled = MutableLiveData(false)
    var lastLocation = MutableLiveData<Location?>(null)
    var heading = MutableLiveData<Double>(0.0)
    var distanceTraveledSinceLastLocation = MutableLiveData<Float>(0F)

    //
    // Public Functions
    //

    fun recordLocation(location: Location) {
        lastLocation.value?.let {
            distanceTraveledSinceLastLocation.value = location.distanceTo(it)
        }
        lastLocation.value = location
    }

    fun setLocationAvailability(value: Boolean) {
        isLocationAvailable.value = value
    }

    fun setBackgroundTrackingAvailability(value: Boolean) {
        isBackgroundTrackingEnabled.value = value
    }

    fun setHeading(value: Double) {
        if (abs(value - (heading.value ?: 0.0)) > 5) {
            heading.value = value
        }
    }
}