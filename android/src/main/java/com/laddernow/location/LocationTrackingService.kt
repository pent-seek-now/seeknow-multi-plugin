package com.laddernow.location

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.android.gms.location.*
import java.util.concurrent.TimeUnit

class LocationTrackingService: Service() {

    companion object {
        val TAG = LocationTrackingService::class.simpleName
        const val SERVICE_ID = 1011
        val UPDATES_EVERY_SECONDS = TimeUnit.SECONDS.toMillis(10)
        val FASTEST_UPDATES_SECONDS = TimeUnit.SECONDS.toMillis(5)

        // these times need to at least as frequent as 10 and 5, maybe even faster
        // so that geofence tracking is more accurate.
    }

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var sensorService: SensorService

    private var isTracking = false
    private var lastLocation: Location? = null

    private var locationRequest: LocationRequest = LocationRequest.create().apply {
        interval = UPDATES_EVERY_SECONDS
        fastestInterval = FASTEST_UPDATES_SECONDS
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun distance(lastLocation: Location, currentLocation: Location) : Float {
        return lastLocation.distanceTo(currentLocation)
    }

    // This handles the receipt of the location updates and checks the location being
    // reported against the last location kept. If the new location is less than the
    // threshold it gets rejected.
    private var locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val loc = locationResult.locations.last()
            //Log.d(TAG, "Location change to: ${loc.latitude}, ${loc.longitude}")
            if (lastLocation == null) {
                LocationTrackingModel.recordLocation(loc)
                lastLocation = loc
            } else {
                lastLocation?.let {
                    if (distance(it, loc) > LocationTrackingModel.distanceThresholdMeters) {
                        LocationTrackingModel.recordLocation(loc)
                        lastLocation = loc
                    }
                }
            }
        }

        override fun onLocationAvailability(availability: LocationAvailability) {
            super.onLocationAvailability(availability)
            Log.d(TAG, "LocationAvailability: ${availability.isLocationAvailable}")
            LocationTrackingModel.setLocationAvailability(availability.isLocationAvailable)
        }
    }

    //
    // Notification
    //
    private fun createNotificationChannel(reason: String, channelID: String, channelName: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelID, channelName, importance).apply {
                description = reason
                lightColor = Color.GREEN
                lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    //
    // Service()
    //

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy called")
        stopLocationUpdates()
        sensorService.stopSensing()
        super.onDestroy()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        val reason = intent?.getStringExtra("reason") ?: "Tracks your location"
        val channelID = intent?.getStringExtra("channelID") ?: "com.seeknow.plugin.channel"
        val channelName = intent?.getStringExtra("channelName") ?: "SeekNow Location Tracker"
        val smallIcon = intent?.getIntExtra("smallIcon", 0) ?: 0

        createNotificationChannel(reason, channelID, channelName)

        startForeground(reason, intent!!, channelID, channelName, smallIcon)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        sensorService = SensorService(this)
        sensorService.startSensing()

        startLocationUpdates()

        return START_STICKY
    }

    private fun startForeground(reason: String?, intent: Intent, channelID: String, channelName: String, smallIcon: Int) {
        val notificationIntent = Intent(intent)
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)

        // Use the notification channel set up by the MainActivity to post a non-cancellable
        // notification that this service is operating.
        val builder = NotificationCompat.Builder(this, channelID)
            .setSmallIcon(smallIcon)
            .setContentTitle(channelName)
            .setContentText(reason)
            .setContentIntent(pendingIntent)

        startForeground(SERVICE_ID, builder.build())
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        if (isTracking) return
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())
        isTracking = true

        Log.d(TAG, "startLocationUpdates")
    }

    private fun stopLocationUpdates() {
        if (!isTracking) return
        fusedLocationClient.removeLocationUpdates(locationCallback)
        isTracking = false

        Log.d(TAG, "stopLocationUpdates")
    }
}