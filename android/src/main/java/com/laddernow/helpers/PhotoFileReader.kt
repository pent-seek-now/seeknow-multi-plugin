package com.laddernow.helpers

import android.content.ContentUris
import android.content.Context
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.util.Log
import com.laddernow.models.Photo

class PhotoFileReader(private val context: Context, private val maxToLoad: Int = 50) {

    companion object {
        private val TAG = PhotoFileReader::class.simpleName
    }

    private val collection: Uri =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL)
        } else {
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        }

    private val projection = arrayOf(
        MediaStore.Images.Media._ID,
        MediaStore.Images.Media.DISPLAY_NAME
    )

    private val photoList = mutableListOf<Photo>()
    private val sortOrder = "${MediaStore.Images.Media.DATE_ADDED} DESC"
    private var currentPosition: Int = -1

    var size: Int = 0

    fun photoAt(position: Int): Photo? {
        if (position < photoList.size) {
            return photoList[position]
        }

        // if there are more photos possible, try and load more
        if (position+1 < size) {
            readPhotos()
            return photoAt(position)
        }

        return null
    }

    fun readPhotos() {
        Log.i(TAG, "Reading photos from position $currentPosition")

        context.contentResolver.query(
            collection,
            projection,
            null,
            null,
            sortOrder
        )?.use { cursor ->
            val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
            val nameColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)

            // always keep size the total number of available items
            size = cursor.count

            cursor.moveToPosition(currentPosition)

            var numImages = 0

            while (cursor.moveToNext() && numImages < maxToLoad) {
                val id = cursor.getLong(idColumn)
                val name = cursor.getString(nameColumn)
                Log.i(TAG, "Reading photo $name")
                val contentUri: Uri =
                    ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id)

                /// Not sure I want to do this here. If there is a dead photo is might be rare and
                /// doing this takes extra time. Maybe only do it when the photo is picked and
                /// and indicate that the photo no longer exists.
//                if (doesUriExist(context, contentUri)) {
//                    photoList += Photo(contentUri, name)
//                }

                // not doing the above displays unreadable images with a special glyph and
                // tapping on this will have no effect. Ive left the code comment in place for
                // now in case it has to be revived.
                photoList += Photo(contentUri, name)

                numImages += 1
            }

            Log.i(TAG, "size = $size, currentPosition = $currentPosition, numImages read = $numImages")
            currentPosition += numImages
        }
    }

    private fun doesUriExist(context: Context, uri: Uri) : Boolean {
        return try {
            val inputStream = context.contentResolver.openInputStream(uri)
            inputStream?.close()
            true
        } catch (e: Exception) {
            Log.e("PhotoFileReader", "Photo does not exist: $uri")
            false
        }
    }
}