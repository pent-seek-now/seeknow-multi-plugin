package com.laddernow.helpers

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.graphics.Color
import android.graphics.Rect
import android.util.AttributeSet
import android.view.*
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.laddernow.plugins.multipurpose.R
import java.lang.Float.max
import java.lang.Float.min
import kotlin.math.abs
import kotlin.math.atan2
import kotlin.math.roundToInt


class ZoomControls @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    companion object {
        const val TAG = "ZoomControls"
        const val minZoomValue = 0.5F
        const val maxZoomValue = 10F
        const val minZoomWiggle = 25F
        const val animationDuration = 250L
    }

    private var selectedZoom: Int = 1
    private var zoomButtons = mutableListOf<Button>()
    private var velocityTracker: VelocityTracker = VelocityTracker.obtain()

    private var zoomValue: Float = 0.25F
    private var lastX: Float = 0F
    private var lastY: Float = 0F
    private var currentAngle: Float = 0f
    private lateinit var currentZoomValue: TextView

    private var zoomChangeHandler: ((Float) -> Unit)? = null

    private val zoomLabels = arrayOf("0.5x", "1x", "2.5x", "10x")
    private val zoomValues = arrayOf(0F, 0.25F, 0.5F, 1F)

    fun setOnZoomChangeHandler(f: (Float)->Unit) {
        this.zoomChangeHandler = f
    }

    fun getCurrentZoom(): Float { return zoomValue }

    // there are three states:
    // - normal: where the zoom presets are visible and useful
    // - zooming: where the user is zooming in and out using their finger
    // - custom: where zooming in/out is over and a custom zoom factor is displayed.
    // tapping the control while in custom mode resets the control to normal state
    // moving the finger will go from custom back to zooming

    enum class ZoomState {
        NORMAL, ZOOMING, CUSTOM
    }

    private var zoomState: ZoomState = ZoomState.NORMAL

    init {
        LayoutInflater.from(context).inflate(R.layout.zoom_controls, this, true)
        setBackgroundResource(R.drawable.zoom_controls_background)
        setPadding(10, 20, 10, 20)
        gravity = Gravity.CENTER
        isClickable = true
        orientation = if (context.resources.configuration.orientation == ORIENTATION_PORTRAIT) HORIZONTAL else VERTICAL

        layoutControls()
    }

    override fun onDetachedFromWindow() {
        velocityTracker.recycle()
        super.onDetachedFromWindow()
    }

    private fun setMargins(onView: View, left: Int, right: Int, top: Int, bottom: Int) {
        (onView.layoutParams as ViewGroup.MarginLayoutParams).apply {
            setMargins(left, right, top, bottom)
        }
    }

    // lays out the controls within its space, a LinearLayout. The orientation of the device determines
    // several factors.

    private fun layoutControls() {
        currentZoomValue = findViewById(R.id.zoom_controls_current)
        currentZoomValue.layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        currentZoomValue.gravity = Gravity.CENTER
        currentZoomValue.visibility = View.GONE
        currentZoomValue.textSize = 18F
        currentZoomValue.textAlignment = TEXT_ALIGNMENT_CENTER
        currentZoomValue.layoutParams.width = 130
        currentZoomValue.layoutParams.height = 130
        setMargins(currentZoomValue, left = 20, right = 20, top = 10, bottom = 10)

        for (i in zoomLabels.indices) {
            val label = zoomLabels[i]
            val button = Button(context)
            button.layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
            setMargins(button, left = 12, top = 12, right = 12, bottom = 12)
            button.layoutParams.width = 110
            button.layoutParams.height = 110
            button.tag = i
            button.text = label
            button.textSize = 12F
            button.setOnClickListener {
                hideSelectedButton()
                selectedZoom = it.tag as Int
                setZoomValue(zoomValues[selectedZoom], true)
                showNormalState()
            }
            button.setBackgroundColor(Color.TRANSPARENT)
            button.setTextColor(Color.YELLOW)

            addView(button)
            button.isClickable = false
            zoomButtons.add(button)
        }

        showSelectedButton()
    }

    fun rotate(degrees: Float) {
        currentAngle = degrees
        zoomButtons.forEach { it.animate().setDuration(125).rotation(degrees).start() }
        currentZoomValue.animate().setDuration(125).rotation(degrees).start()
    }

    // shows or hides the selected zoom preset
    
    private fun hideSelectedButton() {
        if (!(selectedZoom in zoomButtons.indices)) return
        zoomButtons[selectedZoom].setBackgroundColor(Color.TRANSPARENT)
        zoomButtons[selectedZoom].setTextColor(Color.YELLOW)
    }

    private fun showSelectedButton() {
        if (!(selectedZoom in zoomButtons.indices)) return
        zoomButtons[selectedZoom].setBackgroundResource(R.drawable.zoom_selected_background)
        zoomButtons[selectedZoom].setTextColor(Color.BLACK)
    }

    // normal zoom state shows all of the presets with one of the presets selected. note that
    // the normal zoom state restores the presets to their original values. tapping on a preset
    // changes it. dragging your finger across the zoom bar transitions to ZoomingState.

    private fun showNormalState() {
        zoomState = ZoomState.NORMAL
        currentZoomValue.visibility = View.GONE
        for (i in zoomButtons.indices) {
            zoomButtons[i].text = zoomLabels[i]
            zoomButtons[i].visibility = View.VISIBLE
        }
        showSelectedButton()
    }

    // zooming state hides the presets and shows the custom zoom value as the user's finger is
    // dragging across the screen. no selection is shown because no buttons are shown. releasing
    // your finger transitions to CustomState.

    fun showZoomingState() {
        zoomState = ZoomState.ZOOMING
        when (orientation) {
            HORIZONTAL -> {
                ObjectAnimator.ofFloat(this, "translationY", -(this.height + 10F)).apply {
                    duration = animationDuration
                    start()
                }
            }
            VERTICAL -> {
                ObjectAnimator.ofFloat(this, "translationX", -(this.width + 10F)).apply {
                    duration = animationDuration
                    start()
                }
            }
        }
        for (i in zoomButtons.indices) {
            zoomButtons[i].text = zoomLabels[i]
            zoomButtons[i].visibility = View.GONE
        }
        currentZoomValue.visibility = View.VISIBLE
        hideSelectedButton()
    }

    // custom state displays the presets, but one of the preset's labels has been replaced with the
    // custom zoom value and is shown selected. For example, if the custom zoom is 3.6, then the 2x
    // button's label is replaced with "3.6x" since the custom zoom is larger than 2x, but smaller than
    // 4x. dragging your finger over the zoom bar returns to ZoomState. tapping on a preset goes to that
    // setting and restores the button whose label was replaced back to its original value; transitions
    // to NormalState.

    fun showCustomState() {
        when (orientation) {
            HORIZONTAL -> {
                ObjectAnimator.ofFloat(this, "translationY", 0F).apply {
                    duration = animationDuration
                    start()
                }
            }
            VERTICAL -> {
                ObjectAnimator.ofFloat(this, "translationX", 0F).apply {
                    duration = animationDuration
                    start()
                }
            }
        }
        zoomState = ZoomState.CUSTOM
        replaceButtonLabel(zoomValue)

        currentZoomValue.visibility = View.GONE
        zoomButtons.forEach { it.visibility = View.VISIBLE }
        showSelectedButton()
    }

    private fun formattedZoomValue(value: Float): String {
        var result = 0f

        result = when (value) {
            0f -> return "0.5x"
            in 0f..0.2499999999f -> (value / 0.25f) * 0.5f + 0.5f
            in 0.25f..0.599999999f -> ((value - 0.25f) / 0.25f) * 1.5f + 1f
            else -> {
                ((value - 0.5f) / 0.5f) * 7.5f + 2.5f
            }
        }

        val roundedValue = (result * 10f).roundToInt() / 10f
        val nearest = result.roundToInt()
        if (roundedValue == nearest.toFloat()) {
            return "${nearest}x"
        } else {
            return "${roundedValue}x"
        }
    }

    // called during a pinch-to-zoom gesture, the newValue is a floating point number
    // between 0 and 1. This is mapped to our visual scale of 0.5x - 10x
    fun updateZoomValue(newValue: Float) {
        currentZoomValue.text = formattedZoomValue(newValue)
        zoomValue = newValue
    }

    // sets the zoomValue to the newValue and updates the custom zoom text. if a preset was
    // chosen (and stepIt is true), the zoom is "animated" by setting a series of small zoom
    // jumps. This also invokes the zoomChangeHandler if provided. Note that the newValue
    // is in the 0..1 range which is mapped to our visual scale of 0.5x - 10x

    private fun setZoomValue(newValue: Float, stepIt: Boolean = false) {
        currentZoomValue.text = formattedZoomValue(newValue)

        if (stepIt) {
            val steps = (newValue - zoomValue) / 20F
            for (z in 1..20) {
                zoomChangeHandler?.invoke(zoomValue + steps * z)
            }
        }

        zoomValue = newValue
        zoomChangeHandler?.invoke(newValue)
    }

    // handles the touch events on the entire zoom bar:
    // down: resets to begin possible tracking of the finger moving along the bar
    // up: completes the event which depends on whether or not any zooming happened
    // move: once the finger moves beyond the "wiggle" threshold, this event turns
    //  into a continuous zooming interaction.

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                lastX = event.rawX
                lastY = event.rawY
                velocityTracker.clear()
                velocityTracker.addMovement(event)
            }
            MotionEvent.ACTION_UP -> {
                when (zoomState) {
                    ZoomState.NORMAL -> {
                        whichZoomButton(event)?.performClick()
                    }
                    ZoomState.ZOOMING -> {
                        showCustomState()
                    }
                    ZoomState.CUSTOM -> {
                        whichZoomButton(event)?.performClick()
                    }
                }
                lastX = 0F
                lastY = 0F
            }
            MotionEvent.ACTION_MOVE -> {
                velocityTracker.addMovement(event)
                velocityTracker.computeCurrentVelocity(1)

                val speed = if (orientation == HORIZONTAL) velocityTracker.xVelocity else velocityTracker.yVelocity
                val distance = if (orientation == HORIZONTAL) abs(event.rawX - lastX) else abs(event.rawY - lastY)

                if (zoomState != ZoomState.ZOOMING && distance > minZoomWiggle) {
                    showZoomingState()
                }

                if (zoomState == ZoomState.ZOOMING) {
                    val desiredZoomFactor = zoomValue + atan2(speed, 35F)
                    val newZoom = max(0F, min(desiredZoomFactor, 1F))
                    setZoomValue(newZoom)
                }
            }
            else -> {
                // ??
            }
        }

        return super.onTouchEvent(event)
    }

    // determines which zoom preset the event happened over. If a preset aligns with the
    // event, it is returned where the caller will trigger its click event.

    private fun whichZoomButton(event: MotionEvent): Button? {
        val x = event.rawX.roundToInt()
        val y = event.rawY.roundToInt()
        val xFactor = when (currentAngle) {
            90f -> 1
            else -> 0
        }
        val yFactor = when (currentAngle) {
            -90f -> 1
            else -> 0
        }

        fun inViewBounds(view: View, x: Int, y: Int, xFactor: Int, yFactor: Int): Boolean {
            val outRect = Rect()
            val location = IntArray(2)
            view.getDrawingRect(outRect)
            view.getLocationOnScreen(location)

            outRect.offset(location[0], location[1])
            return outRect.contains(x + outRect.width()*xFactor, y + outRect.height()*yFactor)
        }

        for (child in zoomButtons) {
            if (inViewBounds(child, x, y, xFactor, yFactor)) {
                return child
            }
        }

        return null
    }

    // replaces a zoom preset label with a new label based on the `newValue` for the zoom. The
    // zoom value shown is in the 0..1 range.

    private fun replaceButtonLabel(newValue: Float) {
        val test = (newValue * 100f).roundToInt() / 100f
        val count = zoomLabels.size - 1
        for (i in 0 until count) {
            if (zoomValues[i] <= test && test < zoomValues[i+1]) {
                zoomButtons[i].text = formattedZoomValue(test)
                selectedZoom = i
                return
            }
        }
        selectedZoom = count
    }
}