package com.laddernow.helpers

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.util.Size
import androidx.core.net.toUri
import androidx.exifinterface.media.ExifInterface
import com.laddernow.models.ExifMetadata
import com.laddernow.plugins.multipurpose.R
import java.io.File
import java.io.FileOutputStream
import kotlin.math.max

class PhotoFileWriter(val context: Context) {

    private val outputDirectory: File
        get() {
            val mediaDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)?.let {
                File(it, context.resources.getString(R.string.app_name)).apply { mkdirs() } }
            return if (mediaDir != null && mediaDir.exists())
                mediaDir else context.filesDir
        }

    fun createOutputFile(named: String) = File(outputDirectory, named)

    fun compressPhoto(photoUri: Uri, quality: Int, targetWidth: Int, targetHeight: Int, includeDirection: Double? = null): Uri? {
        val inputStream = context.contentResolver.openInputStream(photoUri) ?: return null
        val bitmap = BitmapFactory.decodeStream(inputStream)

        val useWidth = max(targetWidth, targetHeight)
        val bitmapRatio = bitmap.width.toFloat() / bitmap.height.toFloat()
        val useHeight = (useWidth / bitmapRatio).toInt()
        var size = Size(useWidth, useHeight)
        if (bitmap.width < bitmap.height) size = Size(useHeight, useWidth)

        val reduced = Bitmap.createScaledBitmap(bitmap, size.width, size.height, false)
        val outFile = File(outputDirectory, "compressed.jpg")
        try {
            val fOut = FileOutputStream(outFile)
            reduced.compress(Bitmap.CompressFormat.JPEG, quality, fOut)
            fOut.flush()
            fOut.close()
            bitmap.recycle()
            reduced.recycle()

            val originalExif = ExifMetadata(context.contentResolver, photoUri, includeDirection)

//            includeDirection?.let {
//                val imageDirectionAsString = (it * 100).roundToInt().toString() + "/100"
//                originalExif.setAttribute(ExifInterface.TAG_GPS_IMG_DIRECTION, imageDirectionAsString)
//            }

            val exif = ExifInterface(outFile.path)
            originalExif.copyInto(exif, includeDirection)
            exif.saveAttributes()

            return outFile.toUri()
        } catch (e: Exception) {
            Log.e("PhotoLibraryFragment", "Failed to compress photo: ${e.localizedMessage}")
            return null
        }
    }
}