package com.laddernow.helpers

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View

class CameraGrid(context: Context, attributeSet: AttributeSet): View(context, attributeSet) {

    private val mPaint = Paint()
    private val mPath = Path()

    init {
        mPaint.color = Color.parseColor("#44FFFFFF")
        mPaint.style = Paint.Style.STROKE
        mPaint.strokeJoin = Paint.Join.ROUND
        mPaint.strokeCap = Paint.Cap.ROUND
        mPaint.strokeWidth = 2f
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val w = width.toFloat()
        val h = height.toFloat()

        mPath.addRect(0f, 0f, w, h, Path.Direction.CW)
        mPath.moveTo(w/3, 0f)
        mPath.lineTo(w/3, h)
        mPath.moveTo(2*w/3, 0f)
        mPath.lineTo(2*w/3, h)
        mPath.moveTo(0f, h/3)
        mPath.lineTo(w, h/3)
        mPath.moveTo(0f, 2*h/3)
        mPath.lineTo(w, 2*h/3)

        canvas.drawPath(mPath, mPaint)
    }
}