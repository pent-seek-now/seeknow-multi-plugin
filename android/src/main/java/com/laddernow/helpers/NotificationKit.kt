package com.laddernow.helpers

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

/**
 * NotificationKit just consolidates all of the messy code to create a notification
 * channel and to then send notifications on that channel.
 */
object NotificationKit {

    private val TAG = NotificationKit::class.simpleName
    private const val IGNORE_NOTIFICATION = "IgnoreNotification"
    private var notificationId = 1

    // a collection of dead-end receivers so that tapping on a notification won't
    // activate the app.
    private var receivers = mutableMapOf<String, BroadcastReceiver>()

    /* Creates the single notification channel used by this app. Both the notification that the app is
     * tracking location in the background, and the notifications for geofence crossing, are sent via
     * this channel.
     */
    fun createNotificationChannel(context: Context, channelId: String, channelName: String, descriptionText: String) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelId, channelName, importance).apply {
                description = descriptionText
                lightColor = Color.GREEN
                lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            }

            // Register the channel with the system
            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }

        // This quick little receiver is to intercept the taps on notifications sent out
        // from the GeoFenceBroadcastReceiver.
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                Log.i(TAG, "Got this and doing nothing!!: ${intent?.action}")
            }
        }
        context.registerReceiver(receiver, IntentFilter().apply {
            addAction(IGNORE_NOTIFICATION)
        })

        receivers["geofence"] = receiver
    }

    /*
     * Sends a local notification using the channel previously created
     */
    fun sendNotification(context: Context, channelId: String, title: String, content: String, smallIcon: Int) {
        // this intent will be sent to the broadcast receiver created in the MainActivity and it will do nothing,
        // just eat the notification selection.
        val nextIntent = Intent(IGNORE_NOTIFICATION)
        val pendingIntent = PendingIntent.getBroadcast(context, 0, nextIntent, 0)

        val builder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(smallIcon)
            .setContentTitle(title)
            .setContentText(content)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(content))
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)

        // By keeping the notification ID the same, it will overwrite any previous notifications
        // so they won't build up if the user doesn't Ack them.
        with(NotificationManagerCompat.from(context)) {
            // notificationId is a unique int for each notification that you must define
            notify(notificationId++, builder.build()) // or you can leave off ++ if you just want one notification to replace the last.
        }
    }
}