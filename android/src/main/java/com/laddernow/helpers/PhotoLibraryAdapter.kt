package com.laddernow.helpers

import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.laddernow.models.Photo
import com.laddernow.plugins.multipurpose.R

class PhotoLibraryAdapter(private val reader: PhotoFileReader, private val callback: (Photo) -> Unit): RecyclerView.Adapter<PhotoLibraryAdapter.ViewHolder>() {

    companion object {
        private val TAG = PhotoLibraryAdapter::class.simpleName
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val imageView: ImageView = view.findViewById(R.id.list_photo_image)
        val progress: ProgressBar = view.findViewById(R.id.list_photo_progress)

        init {
            imageView.isClickable = true
            progress.isVisible = false
        }
    }

    private var focusedItem: Int = RecyclerView.NO_POSITION

    override fun getItemCount(): Int = reader.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoLibraryAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.photo_list_item, parent, false)
        return PhotoLibraryAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: PhotoLibraryAdapter.ViewHolder, position: Int) {
        holder.itemView.isSelected = focusedItem == position
        holder.progress.isVisible = focusedItem == position

        val photo = reader.photoAt(position) ?: return

        holder.imageView.setOnClickListener {
            if (photo.isBad) return@setOnClickListener
            notifyItemChanged(focusedItem)
            focusedItem = position
            notifyItemChanged(focusedItem)
            callback.invoke(photo)
        }

        holder.imageView.post {
            Glide.with(holder.imageView)
                .load(photo.uri)
                .placeholder(R.drawable.ic_baseline_insert_photo_24)
                .error(R.drawable.ic_baseline_image_not_supported_24)
                .listener(object: RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        Log.e(TAG, "Unable to load image at ${photo.uri}")
                        photo.isBad = true
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        // ignoring this, at least for now
                        return false
                    }
                })
                .into(holder.imageView)
        }
    }
}