package com.laddernow

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.laddernow.helpers.*
import com.laddernow.models.ExifMetadata
import com.laddernow.models.Parameters
import com.laddernow.plugins.multipurpose.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PhotoLibraryActivity : AppCompatActivity() {

//    private val photoReader = PhotoFileReader()
    private val photoLoader = PhotoFileReader(context = this)

    private lateinit var recyclerView: RecyclerView

    private val quality: Int
        get() { return intent.getIntExtra(Parameters.QUALITY.value, 50) }
    private val imageType: String
        get() { return intent.getStringExtra(Parameters.IMAGE_TYPE.value) ?: "jpeg" }
    private val targetWidth: Int
        get() { return intent.getIntExtra(Parameters.TARGET_WIDTH.value, -1) }
    private val targetHeight: Int
        get() { return intent.getIntExtra(Parameters.TARGET_HEIGHT.value, -1) }

    companion object {
        const val TAG = "PhotoLibraryActivity"
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
    }

    class SpacesItemDecoration: RecyclerView.ItemDecoration() {
        val space = 1
        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            outRect.top = space
            outRect.bottom = space
            outRect.left = space
            outRect.right = space
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_library)

        recyclerView = findViewById<RecyclerView>(R.id.library_recyclerview).also {
            it.layoutManager = GridLayoutManager(this, 4, LinearLayoutManager.VERTICAL, false)
            it.addItemDecoration(SpacesItemDecoration())
        }

        findViewById<Button>(R.id.library_cancel_button).also {
            it.setOnClickListener { cancelSelection() }
        }

        if (!allPermissionsGranted()) {
            requestAllPermissions()
        }
    }

    private fun cancelSelection() {
        setResult(RESULT_CANCELED, Intent())
        finish()
    }

    override fun onBackPressed() {
        setResult(RESULT_CANCELED, Intent())
        super.onBackPressed()
    }

    override fun onStart() {
        super.onStart()

        // load in the EXIF metadata tags into their cache as
        // this takes a long time.
        ExifMetadata.loadCache()

        if (allPermissionsGranted()) {
            loadImages()
        }
    }

    //
    // LOAD IMAGES
    //

    private fun loadImages() {
        // provides the initial load and sets the count
        photoLoader.readPhotos()

        val adapter = PhotoLibraryAdapter(photoLoader, callback = { photo ->
            if (photo.isBad) {
                Log.e(TAG, "Bad photo selected. Cancelling selection.")
                cancelSelection()
            } else {
                // write the file in the background, then finish() in the foreground when the write
                // has completed.
                lifecycleScope.launch(Dispatchers.IO) {
                    var photoUri: Uri? = photo.uri

                    if (targetWidth > 0 && targetHeight > 0) {
                        photoUri = PhotoFileWriter(this@PhotoLibraryActivity).compressPhoto(
                            photo.uri,
                            quality,
                            targetWidth,
                            targetHeight
                        )
                    }

                    val intent = Intent()
                    photoUri?.let {
                        intent.putExtra("photoUri", it)
                    } ?: run {
                        Log.e(TAG, "Failed to compress photo.")
                        cancelSelection()
                    }
                    setResult(RESULT_OK, intent)

                    Handler(Looper.getMainLooper()).post {
                        finish()
                    }
                }

            }
        })

        recyclerView.adapter = adapter
    }

    //
    // PERMISSION MANAGEMENT
    //

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            this, it) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestAllPermissions() {
        val requestMultiplePermissions =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { _ ->
                if (allPermissionsGranted()) {
                    loadImages()
                } else {
                    Log.i(TAG, "User did not give permission to access the photo library.")
                    cancelSelection()
                }
            }

        requestMultiplePermissions.launch(REQUIRED_PERMISSIONS)
    }
}