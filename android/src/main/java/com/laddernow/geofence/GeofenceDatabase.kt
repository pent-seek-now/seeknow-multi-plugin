package com.laddernow.models

import android.content.Context
import androidx.room.*


@Dao
interface FenceRecordDao {

    @Query("SELECT * FROM fences")
    suspend fun loadAll(): List<FenceRecord>

    @Insert
    suspend fun insertFence(fence: FenceRecord)

    @Delete
    suspend fun delete(fence: FenceRecord)
}

@Entity(tableName = "fences")
data class FenceRecord(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    var name: String,
    var latitude: Double,
    var longitude: Double,
    var radius: Float,
    var start: Long,             // start date/time in milliseconds
    var duration: Long? = null,  // duration in milliseconds
    var data: String? = null
)

@Database(entities = [FenceRecord::class], version = 1, exportSchema = false)
abstract class GeofenceDatabase : RoomDatabase() {
    abstract fun fenceDataDao(): FenceRecordDao?

    companion object {
        private var instance: GeofenceDatabase? = null
        private const val DATABASE_NAME = "Geofences"

        fun getFenceDatabase(context: Context): GeofenceDatabase? {
            if (instance == null) {
                synchronized(GeofenceDatabase::class) {
                    instance = Room.databaseBuilder(context.applicationContext, GeofenceDatabase::class.java, DATABASE_NAME).build()
                }
            }
            return instance
        }
    }
}