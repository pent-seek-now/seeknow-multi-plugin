package com.laddernow.geofence

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofenceStatusCodes
import com.google.android.gms.location.GeofencingEvent

/**
 * The GeofenceBroadcastReceiver is used to catch geofence crossing
 * events. Once an event is received, it is passed to the GeofenceKit
 * for notification handling
 */
class GeofenceBroadcastReceiver : BroadcastReceiver() {

    companion object {
        val TAG = GeofenceBroadcastReceiver::class.simpleName
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        val intent = intent ?: return

        //
        val geofencingEvent = GeofencingEvent.fromIntent(intent)
        if (geofencingEvent.hasError()) {
            val errorMessage = GeofenceStatusCodes.getStatusCodeString(geofencingEvent.errorCode)
            Log.e(TAG, errorMessage)
            return
        }

        // send a notification for each geofence triggered
        geofencingEvent.triggeringGeofences.forEach {
            val eventType = when(geofencingEvent.geofenceTransition) {
                Geofence.GEOFENCE_TRANSITION_ENTER -> "ENTER"
                Geofence.GEOFENCE_TRANSITION_EXIT -> "EXIT"
                else -> "CROSS"
            }

            GeofenceKit.recordGeofenceCrossing(context!!,
                name = it.requestId,
                eventType = eventType,
                location = geofencingEvent.triggeringLocation)
//            GeofenceKit.recordGeofenceCrossing(context!!,
//                geofencingEvent.geofenceTransition,
//                it.requestId, eventType,
//                geofencingEvent.triggeringLocation)
        }
    }
}