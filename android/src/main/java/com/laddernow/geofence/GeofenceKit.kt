package com.laddernow.geofence

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.location.Location
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.laddernow.models.FenceRecord
import com.laddernow.models.GeofenceDatabase
import com.laddernow.helpers.NotificationKit
import com.laddernow.plugins.multipurpose.R
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

/**
 * GeofenceKit provides the wrapper for creating and managing geofences. This kit is referenced
 * from the main code as well as the GeofenceBroadcastReceiver
 *
 * note: this is a static object and not a class so that its members can be accessed from
 * multiple points without having to create a hack to pass an instance around.
 */

object GeofenceKit {

    //
    // data
    //

    data class GeofenceCrossing(val name: String, val eventType: String, val location: Location, val radius: Float, val timestamp: Date, val data: String?)

    // private data
    private const val TAG = "GeofenceKit"
    private var backingFenceList = ArrayList<FenceRecord>()
    private lateinit var geofencePendingIntent: PendingIntent
    private const val GeofenceChannelId = "GeofenceChannel"
    private lateinit var apiServer: String
    private lateinit var authToken: String

    // database to track geofences
    private var database: GeofenceDatabase? = null

    // public data
    var geofenceEvent = MutableLiveData<GeofenceCrossing?>(null)
    var fenceList = MutableLiveData(backingFenceList)

    //
    // functions
    //

    fun configure(context: Context, apiServer: String, authToken: String ) {
        database = GeofenceDatabase.getFenceDatabase(context)

        this.apiServer = apiServer
        this.authToken = authToken

        NotificationKit.createNotificationChannel(
            context = context, GeofenceChannelId,
            channelName = "SeekNow Geofence Channel",
            descriptionText = "Notifications from geofence crossing events.")

        val intent = Intent(context, GeofenceBroadcastReceiver::class.java)
        intent.action = "Geofence Action"
        geofencePendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        MainScope().launch {
            loadGeofences()
            createAllGeofences(context)
        }
    }

    /**
     * Called from the GeofenceBroadcastReceiver when it gets a geofence crossing event, this function will
     * post the event to the geofenceEvent LiveData (in case some UI needs to react to it) as well as
     * send a location notification about the event using NotificationKit.
     */
    fun recordGeofenceCrossing(context: Context, name: String, eventType: String, location: Location) {
        // this test is unlikely to ever be needed as once the real geofence (in Google land) expires,
        // it will never trigger an event to call this function. But just in case, and to have parity
        // with iOS, I am leaving this here.
        if (hasExpiredThenRemove(context, name)) {
            Log.e(TAG, "Geofence $name has expired!")
            return
        }

        backingFenceList.firstOrNull { it.name == name }?.let {
            val crossing = GeofenceCrossing(
                name = name,
                eventType = eventType,
                location = location,
                radius = it.radius,
                timestamp = Date(),
                data = it.data)

            Log.d(TAG, "GeofenceCrossing at ${crossing.timestamp}: ${crossing.eventType} - '${crossing.name}'")
            geofenceEvent.postValue(crossing)

            NotificationKit.sendNotification(
                context = context,
                channelId = GeofenceChannelId,
                title = "GEOFENCE $eventType",
                content = name,
                smallIcon = R.mipmap.ic_launcher)
        }
    }

    /**
     * Creates a single geofence at the given location with the given radius. The fence is then activated
     * and stored in a local database.
     */
    @SuppressLint("MissingPermission")
    fun saveGeofence(name: String, center: LatLng, radius: Float, expires: Long?, data: String?, context: Context) {

        val fenceData = FenceRecord(
            name = name,
            latitude = center.latitude,
            longitude = center.longitude,
            radius = radius,
            start = Date().time,
            duration = expires,
            data = data)

        createGeofence(fenceData, context)
    }

    /**
     * Removes a geofence with the given name.
     */
    fun removeGeofence(name: String, context: Context) {
        // step 1: find the fenceData in this list
        val fenceRecord = backingFenceList.firstOrNull { it.name == name } ?: return

        // step 2: deactivate the Geofence itself
        val geofencingClient = LocationServices.getGeofencingClient(context)
        geofencingClient.removeGeofences(listOf(name)).run {
            addOnSuccessListener {
                // step 3: update the database and remove the record
                val dao = database?.fenceDataDao()
                MainScope().launch {
                    dao?.delete(fenceRecord)
                }

                // step 4: remove from the backing list
                backingFenceList.remove(fenceRecord)
                fenceList.postValue(backingFenceList)

                Log.e(TAG, "Geofence '${fenceRecord.name}' as been removed!")
            }
            addOnFailureListener {
                Log.e(TAG, "Failed to remove geofence $name")
            }
        }
    }

    /**
     * Removes all stored geofences and deactivates them
     */
    fun removeAllGeofences(context: Context) {
        val geofencingClient = LocationServices.getGeofencingClient(context)

        backingFenceList.forEach { fenceRecord ->
            geofencingClient.removeGeofences(geofencePendingIntent).run {
                addOnSuccessListener {
                    val dao = database?.fenceDataDao()
                    MainScope().launch {
                        dao?.delete(fenceRecord)
                    }
                }
                addOnFailureListener {
                    Log.e(TAG, "Failed to remove fence '${fenceRecord.name}'")
                }
            }
        }

        backingFenceList.removeAll { true }
        fenceList.postValue(backingFenceList)
    }

    /**
     * Just returns all of the geofences currently in the system; active or otherwise
     */
    suspend fun listGeofences(): ArrayList<FenceRecord> {
        loadGeofences()
        return backingFenceList
    }

    fun cleanupExpiredGeofences(context: Context) {
        backingFenceList
            .filter { it.duration != null }
            .filter { isExpired(it) }
            .forEach { removeGeofence(it.name, context) }
    }

    //
    // INTERNAL
    //

    private fun hasExpiredThenRemove(context: Context, name: String): Boolean {
        var didExpire = false
        backingFenceList
            .filter { it.name == name }
            .filter { isExpired(it) }
            .forEach { removeGeofence(it.name, context); didExpire = true }
        return didExpire
    }

    private fun isExpired(record: FenceRecord): Boolean {
        return when (record.duration) {
            null -> false
            else -> Date().after(Date(record.start + (record.duration ?: 0)))
        }
    }

    private suspend fun loadGeofences() {
        backingFenceList.removeAll { true }
        val dao = database?.fenceDataDao()
        dao?.loadAll()?.let { list ->
            for (record in list) {
                backingFenceList.add(record)
            }
        }
        fenceList.postValue(backingFenceList)
    }

    private fun createAllGeofences(context: Context) {
        for (record in backingFenceList) {
            createGeofence(record, context, addToDatabase = false)
        }
    }

    @SuppressLint("MissingPermission")
    private fun createGeofence(record: FenceRecord?, context: Context, addToDatabase: Boolean = true) {
        if (record == null) return

        val useRadius = if (record.radius < 100) 100F else record.radius
        val duration = record.duration ?: Geofence.NEVER_EXPIRE

        val geofence = Geofence.Builder()
            .setRequestId(record.name)
            .setExpirationDuration(duration)
            .setCircularRegion(record.latitude, record.longitude, useRadius)
            .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER or Geofence.GEOFENCE_TRANSITION_EXIT)
            .build()

        val geofencingClient = LocationServices.getGeofencingClient(context)
        geofencingClient.addGeofences(getSingleGeofenceRequest(geofence), geofencePendingIntent).run {
            addOnSuccessListener {
                if (addToDatabase) {
                    backingFenceList.add(record)
                    fenceList.postValue(backingFenceList)

                    val dao = database?.fenceDataDao()
                    MainScope().launch {
                        dao?.insertFence(record)
                    }
                }
            }
            addOnFailureListener {
                Log.e(TAG, "Failed to create geofence for ${record.name}", it)
            }
        }
    }

    private fun getSingleGeofenceRequest(geofence: Geofence): GeofencingRequest {
        return GeofencingRequest.Builder().apply {
            setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
            addGeofence(geofence)
        }.build()
    }
}