package com.laddernow.models

import android.content.ContentResolver
import android.net.Uri
import android.util.Log
import android.util.Rational
import androidx.exifinterface.media.ExifInterface
import androidx.exifinterface.media.ExifInterface.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception
import kotlin.math.abs
import kotlin.math.roundToInt
import kotlin.reflect.KProperty0
import kotlin.reflect.full.staticProperties

class ExifMetadata() {

    private lateinit var exif: ExifInterface

    data class GPSData(val latitude: Double, val longitude: Double, val altitude: Double, val direction: Double)

    val version: String
        get() = exif.getAttribute(TAG_EXIF_VERSION) ?: "Unknown"
    val imageWidth: String
        get() = exif.getAttribute(TAG_IMAGE_WIDTH) ?: "0"
    val imageLength: String
        get() = exif.getAttribute(TAG_IMAGE_LENGTH) ?: "0"
    val colorSpace: String
        get() = exif.getAttribute(TAG_COLOR_SPACE) ?: "Unknown"
    val aperture: String
        get() = exif.getAttribute(TAG_APERTURE_VALUE) ?: "Unknown"
    val fNumber: String
        get() = exif.getAttribute(TAG_F_NUMBER) ?: "Unknown"
    val iso: String
        get() = exif.getAttribute(TAG_ISO_SPEED)  ?: "Unknown"
    val whiteBalance: String
        get() = exif.getAttribute(TAG_WHITE_BALANCE) ?: "Unknown"
    val exposureMode: String
        get() = exif.getAttribute(TAG_EXPOSURE_MODE) ?: "Unknown"
    val exposureTime: String
        get() = exif.getAttribute(TAG_EXPOSURE_TIME) ?: "Unknown"
    val brightness: String
        get() = exif.getAttribute(TAG_BRIGHTNESS_VALUE) ?: "Unknown"
    val focalLength: String
        get() = exif.getAttribute(TAG_FOCAL_LENGTH) ?: "Unknown"
    val latitude: String
        get() = exif.getAttribute(TAG_GPS_LATITUDE) ?: "Unknown"
    val latitudeRef: String
        get() = exif.getAttribute(TAG_GPS_LATITUDE_REF) ?: ""
    val longitude: String
        get() = exif.getAttribute(TAG_GPS_LONGITUDE) ?: "Unknown"
    val longitudeRef: String
        get() = exif.getAttribute(TAG_GPS_LONGITUDE_REF) ?: ""
    val altitude: String
        get() = exif.getAttribute(TAG_GPS_ALTITUDE) ?: "Unknown"
    val altitudeRef: String
        get() = exif.getAttribute(TAG_GPS_ALTITUDE_REF) ?: "?"
    val bearing: String?
        get() = exif.getAttribute(TAG_GPS_DEST_BEARING)
    val bearingRef: String?
        get() = exif.getAttribute(TAG_GPS_DEST_BEARING_REF)
    val orientation: String
        get() = exif.getAttribute(TAG_ORIENTATION) ?: "0"
    val direction: String
        get() = exif.getAttribute(TAG_GPS_IMG_DIRECTION) ?: "Unknown"

    companion object {
        var propsCache: List<KProperty0<*>>? = null
        fun loadCache() {
            GlobalScope.launch {
                if (propsCache == null) {
                    Log.e("EXIF", "Beginning to load the EXIF cache")
                    val exifClass = ExifInterface::class
                    propsCache =
                        exifClass.staticProperties.filter { it.name.startsWith("TAG_", false) }
                    Log.e("EXIF", "Cache has been loaded.")
                }
            }
        }
    }

    constructor(contentResolver: ContentResolver, uri: Uri, heading: Double? = null) : this() {
        val inputStream = contentResolver.openInputStream(uri) ?: return

        exif = ExifInterface(inputStream)

        heading?.let {
            val imageDirectionAsString = (abs(it) * 100).roundToInt().toString() + "/100"
            exif.setAttribute(ExifInterface.TAG_GPS_IMG_DIRECTION, imageDirectionAsString)
        }

        if (propsCache == null) {
            val exifClass = ExifInterface::class
            propsCache = exifClass.staticProperties.filter { it.name.startsWith("TAG_", false) }
        }
    }

    fun setAttribute(tag: String, value: String?) {
        exif.setAttribute(tag, value)
    }

    fun gpsInfo() : String {
        val gpsData = gpsData()
        return "Latitude = ${gpsData.latitude}\n" +
                "Longitude = ${gpsData.longitude}\n" +
                "Altitude = ${gpsData.altitude}\n" +
                "Direction = ${gpsData.direction}"
    }

    fun gpsData() : GPSData {
        val latDouble = convertDMSToDouble(latitude, if (latitudeRef == "N") 1 else -1)
        val lonDouble = convertDMSToDouble(longitude, if (longitudeRef == "E") 1 else -1)
        val altDouble = convertRationalToDouble(altitude)
        val dirDouble = convertRationalToDouble(direction)
        return GPSData(
            latitude = latDouble,
            longitude = lonDouble,
            altitude = altDouble,
            direction = dirDouble)
    }

    private fun convertRationalToDouble(rat: String) : Double {
        val parts = rat.split("/")
        if (parts.size != 2) return 0.0
        val rational = Rational(parts[0].toInt(), parts[1].toInt())
        return rational.toDouble()
    }

    private fun convertDMSToDouble(dms: String, direction: Int) : Double {
        val parts = dms.split(",")
        if (parts.size != 3) return 0.0
        val degrees = parts[0].split("/")[0].toDouble()
        val minutes = parts[1].split("/")[0].toDouble()
        val secondsParts = parts[2].split("/")
        if (secondsParts.size != 2) return 0.0
        val seconds = (secondsParts[0].toDouble()) / (secondsParts[1].toDouble())
        return (degrees + (minutes / 60) + (seconds / 3600)) * direction.toDouble()
    }

    fun exifInfo() : String {
        return "EXIF Version = $version\n" +
                "Orientation = $orientation\n" +
                "Color Space = $colorSpace\n" +
                "Aperture Value = $aperture\n" +
                "F Number = $fNumber\n" +
                "White Balance = $whiteBalance\n" +
                "Exposure Mode = $exposureMode\n" +
                "Exposure Time = $exposureTime\n" +
                "Brightness = $brightness\n" +
                "Focal Length = $focalLength mm\n" +
                "ISO = $iso\n" +
                "Image Width = $imageWidth\n" +
                "Image Length = $imageLength"
    }

    fun copyInto(target: ExifInterface, heading: Double?) {
        for (prop in propsCache!!) {
            try {
                val propValue = prop.get() as String
                exif.getAttribute(propValue)?.let { value ->
                    //Log.d("EXIF", "Property ${prop.name} exists with value $value")
                    target.setAttribute(propValue, value)
                }
            } catch(e: Exception) {
                //Log.e("EXIF", "Property ${prop.name} does not exist. Skipping")
                // ignore, just skip this
            }
        }
    }
}