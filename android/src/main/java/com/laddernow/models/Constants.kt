package com.laddernow.models

enum class Parameters(val value: String) {
    QUALITY("quality"),
    IMAGE_TYPE("imageType"),
    TARGET_WIDTH("targetWidth"),
    TARGET_HEIGHT("targetHeight"),
    PREFERRED_ORIENTATION("preferredOrientation"),
    GRID("showGrid")
}
