package com.laddernow.models

import android.net.Uri
import androidx.camera.core.ImageCapture
import androidx.lifecycle.ViewModel

class CameraState: ViewModel() {
    var flashSetting: Int = ImageCapture.FLASH_MODE_AUTO
    var zoomSetting: Int = 0
    var showingPreview: Boolean = false
    var photoUri: Uri? = null
    var isLandscape: Boolean = false
}