package com.laddernow.models

import android.net.Uri

data class Photo(val uri: Uri,
                 val name: String,
                 var isBad: Boolean = false)