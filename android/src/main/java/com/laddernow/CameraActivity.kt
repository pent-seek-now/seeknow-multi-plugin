package com.laddernow

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.util.Size
import android.view.*
import android.view.animation.*
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.camera.core.Camera
import androidx.camera.core.FocusMeteringAction.FLAG_AE
import androidx.camera.core.FocusMeteringAction.FLAG_AF
import androidx.camera.core.ImageCapture.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.*
import com.laddernow.helpers.CameraGrid
import com.laddernow.helpers.PhotoFileWriter
import com.laddernow.helpers.ZoomControls
import com.laddernow.location.LocationTrackingModel
import com.laddernow.location.SensorService
import com.laddernow.models.CameraState
import com.laddernow.models.ExifMetadata
import com.laddernow.models.Parameters
import com.laddernow.plugins.multipurpose.R
import kotlinx.coroutines.Runnable
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.lang.Integer.min

class CameraActivity : AppCompatActivity() {

    enum class Orientation {
        PORTRAIT, LANDSCAPE_LEFT, LANDSCAPE_RIGHT, UPSIDE_DOWN
    }

    private var imageCapture: ImageCapture? = null
    private var targetRotation: Int = Surface.ROTATION_0
    private var targetSize = Size(DEFAULT_TARGET_WIDTH, DEFAULT_TARGET_HEIGHT)
    private var flashMode = ImageCapture.FLASH_MODE_AUTO
    private var zoomIndex = ZOOM_1X
    private var cameraReady: Boolean = false

    private lateinit var camera: Camera
    private lateinit var orientationEventListener: OrientationEventListener
    private var currentOrientation: Orientation = Orientation.PORTRAIT
    private lateinit var sensorService: SensorService

    private var photoUri: Uri? = null
    private var scaledBitmap: Bitmap? = null
    private var cameraState: CameraState? = null
    private var photoInProgress = false

    private lateinit var cameraSheet: View
    private lateinit var cameraCaptureButton: ImageButton
    private lateinit var cameraUseButton: Button
    private lateinit var cameraRetakeButton: Button
    private lateinit var cameraCancelButton: Button
    private lateinit var cameraFlashControls: FrameLayout
    private lateinit var cameraFlashOff: ImageButton
    private lateinit var cameraFlashOn: ImageButton
    private lateinit var cameraFlashAuto: Button
    private lateinit var cameraZoomControls: ZoomControls
    private lateinit var cameraConfirmPhotoView: ImageView
    private lateinit var cameraGridView: CameraGrid
    private lateinit var cameraFocusImage: ImageView
    private lateinit var viewFinder: PreviewView
    private lateinit var orientationMaskPanel: ConstraintLayout
    private lateinit var orientationMaskText: TextView

    private val quality: Int
        get() { return intent.getIntExtra(Parameters.QUALITY.value, 50) }
    private val imageType: String
        get() { return intent.getStringExtra(Parameters.IMAGE_TYPE.value) ?: "jpeg" }
    private val targetWidth: Int
        get() { return intent.getIntExtra(Parameters.TARGET_WIDTH.value, 0) }
    private val targetHeight: Int
        get() { return intent.getIntExtra(Parameters.TARGET_HEIGHT.value, 0) }
    private val preferredOrientation: String
        get() { return intent.getStringExtra(Parameters.PREFERRED_ORIENTATION.value) ?: "any"}
    private val showGrid: Boolean
        get() { return intent.getBooleanExtra(Parameters.GRID.value, false)}

    private var useWidth = DEFAULT_TARGET_WIDTH
    private var useHeight = DEFAULT_TARGET_HEIGHT
    private var previousRotation: Int = 0

    private val lastLocation: Location?
        get() = LocationTrackingModel.lastLocation.value

    private val azimuth: Double
        get() = LocationTrackingModel.heading.value ?: 0.0

    companion object {
        private const val TAG = "CameraFragment"
        private const val DEFAULT_TARGET_WIDTH = 960
        private const val DEFAULT_TARGET_HEIGHT = 1280
        private const val ANIMATION_FAST_MILLIS = 50L
        private const val ANIMATION_SLOW_MILLIS = 100L
        private const val ZOOM_1X = 0
        private const val ZOOM_2X = 1
        private const val ZOOM_MAX = 2
        private val REQUIRED_PERMISSIONS = arrayOf(
            Manifest.permission.CAMERA)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)

        cameraState = ViewModelProvider(this).get(CameraState::class.java)

        useWidth = min(targetWidth, targetHeight)
        useHeight = (useWidth * 3) / 4

        cameraSheet = findViewById(R.id.camera_sheet)
        cameraCaptureButton = findViewById(R.id.camera_capture_button)
        cameraUseButton = findViewById(R.id.camera_use_button)
        cameraRetakeButton = findViewById(R.id.camera_retake_button)
        cameraCancelButton = findViewById(R.id.camera_cancel_button)
        cameraFlashControls = findViewById(R.id.camera_flash_controls)
        cameraFlashOff = findViewById(R.id.camera_flash_off)
        cameraFlashOn = findViewById(R.id.camera_flash_on)
        cameraFlashAuto = findViewById(R.id.camera_flash_auto)
        cameraZoomControls = findViewById(R.id.camera_zoom_controls)
        cameraConfirmPhotoView = findViewById(R.id.camera_confirm_photo_view)
        cameraGridView = findViewById(R.id.camera_grid_view)
        cameraFocusImage = findViewById(R.id.camera_focus)
        viewFinder = findViewById(R.id.viewFinder)
        orientationMaskPanel = findViewById(R.id.camera_preferred_orientation_panel)
        orientationMaskText = findViewById(R.id.camera_orientation_instructions)

        orientationEventListener = object : OrientationEventListener(this) {
            override fun onOrientationChanged(orientation: Int) {
                if (orientation == -1) return

                val rotation : Int = when (orientation) {
                    in 45..134 -> Surface.ROTATION_270
                    in 135..224 -> Surface.ROTATION_180
                    in 225..314 -> Surface.ROTATION_90
                    else -> Surface.ROTATION_0
                }
                if (rotation == previousRotation) return
                previousRotation = rotation

                val newOrientation : Orientation = when (orientation) {
                    in 45..134 -> Orientation.LANDSCAPE_RIGHT
                    in 135..224 -> Orientation.UPSIDE_DOWN
                    in 225..314 -> Orientation.LANDSCAPE_LEFT
                    else -> Orientation.PORTRAIT
                }

                if (newOrientation != currentOrientation) {
                    println("Rotation is $newOrientation")
                    currentOrientation = newOrientation
                    rotateEverything()
                }

                val size : Size = when (rotation) {
                    Surface.ROTATION_90 -> Size(useWidth, useHeight)
                    Surface.ROTATION_0 -> Size(useHeight, useWidth)
                    Surface.ROTATION_270 -> Size(useWidth, useHeight)
                    else -> Size(useHeight, useWidth)
                }
                cameraState?.isLandscape = when (rotation) {
                    Surface.ROTATION_270 -> true
                    Surface.ROTATION_90 -> true
                    else -> false
                }
                targetRotation = rotation
                targetSize = size

                handleOrientationMask()
            }
        }

        sensorService = SensorService(this)

        cameraUseButton.visibility = View.INVISIBLE
        cameraConfirmPhotoView.visibility = View.INVISIBLE
        cameraRetakeButton.visibility = View.INVISIBLE
        cameraCancelButton.visibility = View.VISIBLE
        cameraGridView.visibility = if (showGrid) View.VISIBLE else View.INVISIBLE

        cameraCaptureButton.setOnClickListener { takePhoto() }
        cameraRetakeButton.setOnClickListener { retake() }
        cameraUseButton.setOnClickListener { usePhoto() }
        cameraCancelButton.setOnClickListener { cancelCapture() }

        cameraFlashAuto.setOnClickListener { toggleFlashControls(FLASH_MODE_OFF) }
        cameraFlashOff.setOnClickListener { toggleFlashControls(FLASH_MODE_ON) }
        cameraFlashOn.setOnClickListener { toggleFlashControls(FLASH_MODE_AUTO) }

        cameraZoomControls.setOnZoomChangeHandler { zoomValue -> // zoomValue is 0..1
            zoomTo(zoomValue)
        }

        handleOrientationMask()
    }

    override fun dispatchKeyEvent(event: KeyEvent?): Boolean {
        if (photoInProgress) return super.dispatchKeyEvent(event)
        event?.let {
            if (it.keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || it.keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
                if (it.action == KeyEvent.ACTION_DOWN) {
                    takePhoto()
                }
            }
        }
        return super.dispatchKeyEvent(event)
    }

    private fun rotateEverything() {
        val angle: Float = when (currentOrientation) {
            Orientation.LANDSCAPE_LEFT -> 90f
            Orientation.LANDSCAPE_RIGHT -> -90f
            else -> 0f
        }

        cameraCancelButton.animate().setDuration(125).rotation(angle).start()
        cameraFlashControls.animate().setDuration(125).rotation(angle).start()
        cameraRetakeButton.animate().setDuration(125).rotation(angle).start()
        cameraUseButton.animate().setDuration(125).rotation(angle).start()
        cameraZoomControls.rotate(angle)

        scaledBitmap?.let {
            cameraConfirmPhotoView.setImageBitmap(it.rotate(angle))
        }
    }

    private fun toggleFlashControls(newMode: Int) {
        when (newMode) {
            FLASH_MODE_AUTO -> {
                cameraFlashAuto.visibility = View.VISIBLE
                cameraFlashOff.visibility = View.INVISIBLE
                cameraFlashOn.visibility = View.INVISIBLE
            }
            FLASH_MODE_ON -> {
                cameraFlashAuto.visibility = View.INVISIBLE
                cameraFlashOff.visibility = View.INVISIBLE
                cameraFlashOn.visibility = View.VISIBLE
            }
            FLASH_MODE_OFF -> {
                cameraFlashAuto.visibility = View.INVISIBLE
                cameraFlashOff.visibility = View.VISIBLE
                cameraFlashOn.visibility = View.INVISIBLE
            }
        }
        flashMode = newMode
    }

    private fun handleOrientationMask() {
        val isLandscape = cameraState?.isLandscape ?: false

        when (preferredOrientation) {
            "landscape" -> {
                orientationMaskText.setText(R.string.use_landscape)
                orientationMaskPanel.visibility = if (isLandscape) View.GONE else View.VISIBLE
            }
            "portrait" -> {
                orientationMaskText.setText(R.string.use_portrait)
                orientationMaskPanel.visibility = if (isLandscape) View.VISIBLE else View.GONE
            }
            else -> {
                orientationMaskPanel.visibility = View.GONE
            }
        }
    }

    override fun onStart() {
        super.onStart()

        // Request camera and location permissions
        if (allPermissionsGranted()) {
            startCamera()
        } else {
            requestAllPermissions()
        }

        // load in the EXIF metadata tags into their cache as
        // this takes a long time.
        ExifMetadata.loadCache()
    }

    override fun onBackPressed() {
        setResult(RESULT_CANCELED, Intent())
        super.onBackPressed()
    }

    override fun onPause() {
        super.onPause()

        cameraState?.let {
            it.flashSetting = flashMode
            it.zoomSetting = zoomIndex
            it.showingPreview = photoUri != null
            it.photoUri = photoUri
        }

        orientationEventListener.disable()
        sensorService.stopSensing()
    }

    override fun onResume() {
        super.onResume()

        if (cameraState?.showingPreview == true) {
            cameraState?.photoUri?.let { confirmPhoto(it) }
        }

        sensorService.startSensing()

        if (orientationEventListener.canDetectOrientation()) {
            orientationEventListener.enable()
        }
    }

    //
    // ACTIONS
    //

    private fun confirmPhoto(savedUri: Uri) {
        this.photoUri = savedUri

        viewFinder.visibility = View.INVISIBLE
        cameraCaptureButton.alpha = 0.5f
        cameraCaptureButton.isEnabled = false
        cameraFlashControls.alpha = 0.5f
        cameraFlashControls.isEnabled = false
        cameraZoomControls.visibility = View.INVISIBLE
        cameraCancelButton.visibility = View.INVISIBLE
        cameraGridView.visibility = View.INVISIBLE
        cameraConfirmPhotoView.visibility = View.VISIBLE
        cameraUseButton.visibility = View.VISIBLE
        cameraRetakeButton.visibility = View.VISIBLE

        cameraConfirmPhotoView.setImageDrawable(null)
        cameraConfirmPhotoView.setImageURI(savedUri)

        val drawable = cameraConfirmPhotoView.drawable
        scaledBitmap = drawable.toBitmap()

        val angle: Float = when (currentOrientation) {
            Orientation.LANDSCAPE_LEFT -> 90f
            Orientation.LANDSCAPE_RIGHT -> -90f
            else -> 0f
        }

        scaledBitmap?.let {
            cameraConfirmPhotoView.setImageBitmap(it.rotate(angle))
        }
    }

    private fun usePhoto() {
        val uri = photoUri ?: return

        photoInProgress = false

        if (useWidth == 0 || useHeight == 0) {
            val intent = Intent()
            intent.putExtra("photoUri", uri)
            setResult(RESULT_OK, intent)
            finish()
        } else {
            runBlocking {
                launch {
                    val fileWriter = PhotoFileWriter(this@CameraActivity)
                    val compressedUri = fileWriter.compressPhoto(uri, quality, useWidth, useHeight, azimuth)

                    Handler(Looper.getMainLooper()).post {
                        val intent = Intent()
                        intent.putExtra("photoUri", compressedUri)
                        setResult(RESULT_OK, intent)
                        finish()
                    }
                }
            }
        }
    }

    private fun retake() {
        cameraRetakeButton.visibility = View.INVISIBLE
        cameraUseButton.visibility = View.INVISIBLE
        cameraCaptureButton.alpha = 1.0f
        cameraCaptureButton.isEnabled = true
        cameraCancelButton.visibility = View.VISIBLE
        cameraFlashControls.alpha = 1.0f
        cameraFlashControls.isEnabled = true
        cameraZoomControls.visibility = View.VISIBLE
        cameraGridView.visibility = if (showGrid) View.VISIBLE else View.INVISIBLE
        viewFinder.visibility = View.VISIBLE
        cameraConfirmPhotoView.visibility = View.INVISIBLE
        photoUri = null
        photoInProgress = false
    }

    private fun cancelCapture() {
        photoInProgress = false
        setResult(RESULT_CANCELED, Intent())
        finish()
    }

    private fun zoomTo(scale: Float) {
        if (cameraReady) {
            camera.cameraControl.setLinearZoom(scale)
        }
    }

    fun clearPhotoFiles() {
        //TODO
    }

    //
    // CAMERA
    //

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)

        cameraProviderFuture.addListener(Runnable {
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            // Preview: we let CameraX decide on the best camera features to use as
            // the default in this device.
            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(viewFinder.surfaceProvider)
                }

            // ImageCapture: we let CameraX decide the best properties to use to
            // get the base quality photo.
            imageCapture = ImageCapture.Builder()
                .setCaptureMode(CAPTURE_MODE_MAXIMIZE_QUALITY)
                .build()

            // Select back camera as a default
            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera
                camera = cameraProvider.bindToLifecycle(
                    this, cameraSelector, preview, imageCapture)

                cameraReady = true

                // hide the flash controls if this device does not have a flash
                cameraFlashControls.visibility = if (camera.cameraInfo.hasFlashUnit()) View.VISIBLE else View.INVISIBLE

                toggleFlashControls(cameraState?.flashSetting ?: FLASH_MODE_AUTO)

                setupTapToFocusAndPinchToZoom()

            } catch(exc: Exception) {
                Log.e(TAG, "Use case binding failed", exc)
                cancelCapture()
            }

        }, ContextCompat.getMainExecutor(this))
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupTapToFocusAndPinchToZoom() {

        var isScaling = false

        // this scale gesture handler will be invoked when two fingers come into play
        val scaleGestureDetector = ScaleGestureDetector(this,
            object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
                override fun onScaleBegin(detector: ScaleGestureDetector?): Boolean {
                    cameraZoomControls.showZoomingState()
                    return super.onScaleBegin(detector)
                }

                override fun onScaleEnd(detector: ScaleGestureDetector?) {
                    val scale = camera.cameraInfo.zoomState.value!!.zoomRatio * (detector?.scaleFactor ?: 1F)
                    cameraZoomControls.updateZoomValue(camera.cameraInfo.zoomState.value!!.linearZoom)
                    camera.cameraControl.setZoomRatio(scale)
                    cameraZoomControls.showCustomState()
                    super.onScaleEnd(detector)
                }
                override fun onScale(detector: ScaleGestureDetector): Boolean {
                    val scale = camera.cameraInfo.zoomState.value!!.zoomRatio * detector.scaleFactor
                    cameraZoomControls.updateZoomValue(camera.cameraInfo.zoomState.value!!.linearZoom)
                    camera.cameraControl.setZoomRatio(scale)
                    return true
                }
            })

        viewFinder.setOnTouchListener(View.OnTouchListener { view: View, motionEvent: MotionEvent ->
            when ((motionEvent.action and MotionEvent.ACTION_MASK)) {
                MotionEvent.ACTION_DOWN -> {
                    isScaling = false
                    return@OnTouchListener true
                }
                MotionEvent.ACTION_POINTER_DOWN -> {
                    if (motionEvent.pointerCount > 1) {
                        isScaling = true
                        scaleGestureDetector.onTouchEvent(motionEvent)
                    }
                    return@OnTouchListener true
                }
                MotionEvent.ACTION_MOVE -> {
                    if (motionEvent.pointerCount > 1) {
                        scaleGestureDetector.onTouchEvent(motionEvent)
                    }
                    return@OnTouchListener true
                }
                MotionEvent.ACTION_UP -> {
                    if (isScaling) {
                        scaleGestureDetector.onTouchEvent(motionEvent)
                    } else {
                        val factory = viewFinder.meteringPointFactory
                        val point = factory.createPoint(motionEvent.x, motionEvent.y)
                        val action = FocusMeteringAction.Builder(point, FLAG_AF or FLAG_AE).build()
                        val future = camera.cameraControl.startFocusAndMetering(action)
                        showFocus(getRelativePosition(
                            findViewById(R.id.camera_focus_layout),
                            motionEvent
                        ), false)
                        future.addListener({
                            showFocus(
                                getRelativePosition(
                                    findViewById(R.id.camera_focus_layout),
                                    motionEvent
                                ), true)
                        }, ContextCompat.getMainExecutor(this))
                    }
                    return@OnTouchListener true
                }
                else -> return@OnTouchListener false
            }
        })

        // now set the camera's zoom to whatever is the current zoom value
        zoomTo(cameraZoomControls.getCurrentZoom())
    }

    private fun getRelativePosition(v: View, event: MotionEvent): Point? {
        val location = IntArray(2)
        v.getLocationOnScreen(location)
        val screenX = event.rawX
        val screenY = event.rawY
        val viewX = screenX - location[0]
        val viewY = screenY - location[1]
        return Point(viewX.toInt(), viewY.toInt())
    }

    private fun showFocus(point: Point?, animate: Boolean) {
        val point = point ?: return
        cameraFocusImage.visibility = View.VISIBLE
        cameraFocusImage.translationY = point.y.toFloat() - cameraFocusImage.height / 2
        cameraFocusImage.translationX = point.x.toFloat() - cameraFocusImage.width / 2
        cameraFocusImage.scaleX = 1f
        cameraFocusImage.scaleY = 1f
        if (animate) {
            cameraFocusImage.animate()
                .setInterpolator(LinearInterpolator())
                .scaleX(0.2f)
                .scaleY(0.2f)
                .withEndAction {
                    cameraFocusImage.visibility = View.INVISIBLE
                }
                .start()
        }
    }

    private fun takePhoto() {
        // Get a stable reference of the modifiable image capture use case
        val imageCapture = imageCapture ?: return

        photoInProgress = true

        imageCapture.targetRotation = targetRotation
        imageCapture.flashMode = flashMode

        val metadata = ImageCapture.Metadata().apply {
            if (lastLocation != null) {
                location = lastLocation
            }
        }

        // Create time-stamped output file to hold the image
        val photoFile = PhotoFileWriter(this).createOutputFile("photo.jpg")

        // Create output options object which contains file + metadata
        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile)
            .setMetadata(metadata)
            .build()

        Log.i(TAG, "Setting Metadata.bearing to to ${metadata.location?.bearing}")

        val autoFocusPoint = SurfaceOrientedMeteringPointFactory(1f, 1f)
            .createPoint(.5f, .5f)

        // Turn on the torch (flash) and set the metering action to auto-focus and auto-exposure
        camera.cameraControl.startFocusAndMetering(
            FocusMeteringAction.Builder(autoFocusPoint)
                .build()
        )

        // Set up image capture listener, which is triggered after photo has
        // been taken
        imageCapture.takePicture(
            outputOptions, ContextCompat.getMainExecutor(this), object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.e(TAG, "Photo capture failed: ${exc.message}", exc)
                    cancelCapture()
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    val savedUri = Uri.fromFile(photoFile)
                    camera.cameraControl.enableTorch(false)

                    confirmPhoto(savedUri)
                }
            })

        // Display flash animation to indicate that photo was captured
        cameraSheet.postDelayed({
            cameraSheet.foreground = ColorDrawable(Color.WHITE)
            cameraSheet.postDelayed(
                { cameraSheet.foreground = null }, ANIMATION_FAST_MILLIS
            )
        }, ANIMATION_SLOW_MILLIS)
    }

    //
    // PERMISSION MANAGEMENT
    //

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            this, it) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestAllPermissions() {
        val requestMultiplePermissions =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
                if (allPermissionsGranted()) {
                    startCamera()
                } else {
                    Log.i(TAG, "User did not give permission to either use the camera or get their location.")
                    cancelCapture()
                }
            }

        requestMultiplePermissions.launch(REQUIRED_PERMISSIONS)
    }

}


// extension function to rotate bitmap
fun Bitmap.rotate(degrees:Float = 180F): Bitmap? {
    val matrix = Matrix()
    matrix.postRotate(degrees)

    return Bitmap.createBitmap(
        this, // source bitmap
        0, // x coordinate of the first pixel in source
        0, // y coordinate of the first pixel in source
        width, // The number of pixels in each row
        height, // The number of rows
        matrix, // Optional matrix to be applied to the pixels
        false // true if the source should be filtered
    )
}