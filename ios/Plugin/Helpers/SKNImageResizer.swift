//
//  ImageResizer.swift
//  PhotoBrowser
//
//  Created by Peter Ent on 4/6/21.
//

import UIKit

public struct SKNImageResizer {
    
    enum ImageResizingError: Error {
        case cannotRetrieveFromURL
        case cannotRetrieveFromData
    }
    
    var targetWidth: CGFloat = -1
    
    public func resize(at url: URL) -> UIImage? {
        guard let image = UIImage(contentsOfFile: url.path) else {
            return nil
        }
        
        return self.resize(image: image)
    }
    
    public func resize(image: UIImage) -> UIImage {
        let originalSize = image.size
        let targetSize = CGSize(width: targetWidth, height: targetWidth*originalSize.height/originalSize.width)
        let format = UIGraphicsImageRendererFormat()
        format.scale = 1.0
        let renderer = UIGraphicsImageRenderer(size: targetSize, format: format)
        return renderer.image { (context) in
            image.draw(in: CGRect(origin: .zero, size: targetSize))
        }
    }
    
    public func jpegResize(image: UIImage, quality: CGFloat) -> Data {
        let originalSize = image.size
        let targetSize = CGSize(width: targetWidth, height: targetWidth*originalSize.height/originalSize.width)
        let format = UIGraphicsImageRendererFormat()
        format.scale = 1.0
        let renderer = UIGraphicsImageRenderer(size: targetSize, format: format)
        return renderer.jpegData(withCompressionQuality: quality) { context in
            image.draw(in: CGRect(origin: .zero, size: targetSize))
        }
    }
    
    public func jpegOnly(image: UIImage, quality: CGFloat) -> Data {
        let format = UIGraphicsImageRendererFormat()
        format.scale = 1.0
        let renderer = UIGraphicsImageRenderer(size: image.size, format: format)
        return renderer.jpegData(withCompressionQuality: quality) { context in
            image.draw(in: CGRect(origin: .zero, size: image.size))
        }
    }
    
    public func resize(data: Data) -> UIImage? {
        guard let image = UIImage(data: data) else {return nil}
        return resize(image: image )
    }
}
