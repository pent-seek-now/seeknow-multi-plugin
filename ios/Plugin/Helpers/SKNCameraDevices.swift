//
//  SKNCameraDevices.swift
//  PhotoPlugin
//
//  Created by PETER ENT on 5/19/21.
//

import Foundation
import AVFoundation

final public class SKNCameraDevices {
    
    struct ZoomInfo {
        let zoomLevels: [CGFloat]
        let zoomMin: CGFloat = 1
        let zoomMax: CGFloat
        let zoomIndex: Int
    }
    
    // backCamera - the best rear-facing camera for this device
    var backCamera: AVCaptureDevice?
    
    // backZoomInfo - all the details about zoom with the back camera
    var backZoomInfo: ZoomInfo?
    
    // use this as the default camera
    var defaultCamera: AVCaptureDevice? {
        return backCamera
    }
        
    init() {
        let backDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInTripleCamera, .builtInDualCamera, .builtInWideAngleCamera],
                                                                    mediaType: .video,
                                                                    position: .back)
        backCamera = backDiscoverySession.devices.first
        determineZoomLevels(device: backCamera)
    }
    
    // This function is supposed to use the information in the AVCaptureDevice to determine the zoom levels
    // to present in the ZoomControl bar as preset buttons. I cannot figure out of this information is
    // even useful. I'm inclined to believe that Apple has a giant table in its Camera.app which lists
    // every supported iPhone/iPad in the current OS and determines its zoom capabilities that way.
    
    // I am leaving all of this code in comments in case it may be necessary to go back and do something
    // based on the virtual camera type selected. For now, the zoom values are fixed across all iOS
    // devices.
    
    private func determineZoomLevels(device: AVCaptureDevice?) {
        /* -------
        guard let device = device else { return }
        
        let format = device.activeFormat
        print("Min zoom: 1, Max zoom: \(format.videoMaxZoomFactor)")

        var zoomMax: CGFloat = 12
        var zoomIndex = 0
        var zoomLevels = [ZoomPair]() <--- ZoomPair no longer exists if you uncomment this code; now its just zoomLevels:[CGFloat]

        /// how to display 0.5x, 1x, 2.5x but really set the zoom to 1, 2, 5 all the while
        /// zooming continuously?

        switch device.deviceType {
        case .builtInTripleCamera:
            zoomLevels = [(level: 1, label: "0.5x")]
            zoomIndex = 1
        case .builtInDualCamera:
            zoomLevels = [(level: 1, label: "0.5x")]
        default:
            zoomLevels = [(level: 1, label: "1x")]
            zoomMax = 10
        }

        // these values are the zoom factors at which one camera is switched to another
        // at the indicated zoom. For example, at 5x zoom, the normal camera is switched
        // for the telephoto camera.
        let num = device.virtualDeviceSwitchOverVideoZoomFactors
        for value in num {
            zoomLevels.append( (level: CGFloat(value.floatValue), label: "\(value.floatValue/2)x"))
        }
        ------ */
        
        // Fix the zoom levels to this, based on the Pro Max camera system. Every device should be capable
        // of zooming from 1 (its widest angle camera) to 10. The device might be capable of more (like the
        // iPhone 12 Pro Max has a Camera.app max zoom of 12).
        
        backZoomInfo = ZoomInfo(zoomLevels: [1, 2, 5, 10], zoomMax: 10, zoomIndex: 1)
        
//        if device.isVirtualDevice {
//            let realDevices = device.constituentDevices
//            print("These are the real devices: \(realDevices)")
//            for dev in realDevices {
//                print("Device: \(dev.localizedName)")
//                print("  - lensAperture: \(dev.lensAperture)")
//                print("  - active format: \(dev.activeFormat)")
//                print("  - zoom from: \(dev.minAvailableVideoZoomFactor) to \(dev.maxAvailableVideoZoomFactor)")
//                print("  - field of view: \(dev.activeFormat.videoFieldOfView)")
//                print("  - formats:")
//                for format in dev.formats {
//                    print("    - \(format)")
//                }
//            }
//        }
    }
}
