//
//  SKNCameraGrid.swift
//  MultipurposePlugin
//
//  Created by PETER ENT on 10/14/21.
//

import UIKit

class SKNCameraGrid: UIView {
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let path = UIBezierPath(rect: rect)
        path.move(to: CGPoint(x: rect.width/3, y: 0))
        path.addLine(to: CGPoint(x: rect.width/3, y: rect.height))
        path.move(to: CGPoint(x: 2*rect.width/3, y: 0))
        path.addLine(to: CGPoint(x: 2*rect.width/3, y: rect.height))
        path.move(to: CGPoint(x: 0, y: rect.height/3))
        path.addLine(to: CGPoint(x: rect.width, y: rect.height/3))
        path.move(to: CGPoint(x: 0, y: 2*rect.height/3))
        path.addLine(to: CGPoint(x: rect.width, y: 2*rect.height/3))
        
        let layer : CAShapeLayer = CAShapeLayer()
        layer.strokeColor = UIColor(white: 1.0, alpha: 0.25).cgColor
        layer.lineWidth = 1.0
        layer.fillColor = UIColor.clear.cgColor
        layer.path = path.cgPath
        
        self.layer.addSublayer(layer)
    }
}
