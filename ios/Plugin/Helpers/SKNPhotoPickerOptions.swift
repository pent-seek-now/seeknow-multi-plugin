//
//  SKNPhotoPickerOptions.swift
//  PhotoPicker
//
//  Created by PETER ENT on 5/24/21.
//

import UIKit

public struct SKNPhotoPickerOptions {
    
    public enum PreferredOrientation: String {
        case any = "any"
        case portrait = "portrait"
        case landscape = "landscape"
        
        var asLabel: String {
            switch self {
            case .any: return ""
            case .portrait: return "Use Portrait"
            case .landscape: return "Use Landscape"
            }
        }
    }
    
    public let createJpegOnly: Bool
    public let jpegQuality: CGFloat
    public let targetSize: CGSize
    public let preferredOrientation: PreferredOrientation
    public let showGrid: Bool
    
    public init(createJpegOnly: Bool = false, jpegQuality: CGFloat = 0.5, targetSize: CGSize = .zero, preferredOrientation: PreferredOrientation = .any, showGrid: Bool = false) {
        self.createJpegOnly = createJpegOnly
        self.jpegQuality = jpegQuality
        self.targetSize = targetSize
        self.preferredOrientation = preferredOrientation
        self.showGrid = showGrid
    }
}
