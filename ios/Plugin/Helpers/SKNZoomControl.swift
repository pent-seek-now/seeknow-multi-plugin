//
//  SKNZoomControl.swift
//  MultipurposePlugin
//
//  Created by PETER ENT on 9/22/21.
//

import UIKit

/*
 * SKNZoomControl is our custom set of buttons to control the zoom on the camera.
 * The selectedIndex is which zoom setting is currently active.
 *
 * This control automatically reponds to device orientation changes and spins the
 * individual zoom buttons.
 */

@IBDesignable
class SKNZoomControl: UIControl {
    
    private enum ZoomStatus {
        case normal
        case zooming
        case customZoom
    }
    
    var selectedIndex = 0
    var zoomValue: CGFloat = 1
    
    private var shadow: UIView!
    private var selectedBackground: UIView!
    private var zoomInfo: SKNCameraDevices.ZoomInfo?
    private var zoomButtons = [UIButton]()
    private var customZoomLabel: UILabel!
    
    private var status: ZoomStatus = .normal
    private let cornerRadius: CGFloat = 8
    private let spacing: CGFloat = 10
    private let buttonSize: CGFloat = 40
    private let padding: CGFloat = 4
    private var maxButtonWidth: CGFloat = 0
    private var deviceOrientation = UIDeviceOrientation.portrait
    private var controlEvent: UIControl.Event?
    private var prevZoomHit: CGFloat = 0
    private var basePinchZoom: CGFloat = 0
    
    private let generator = UISelectionFeedbackGenerator()
    
    convenience init(info: SKNCameraDevices.ZoomInfo?) {
        self.init(frame: .zero)
        self.zoomInfo = info
        self.selectedIndex = info?.zoomIndex ?? 0
        createContent()
    }
    
    override init(frame rect: CGRect) {
        super.init(frame: rect)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        createContent()
    }
    
    // MARK: - UICONTROL OVERRIDES
    
    // provide the intrinsicContentSize for those times when the parent UIView does not
    // explicitly set the size; useful in AutoLayout situations.
    
    override var intrinsicContentSize: CGSize {
        let width = padding + (buttonSize + spacing) * CGFloat(zoomButtons.count)
        let height = buttonSize + 2 * padding
                
        return CGSize(width: width, height: height)
    }
    
    // intercept the addTarget so it can be invoked when the zoom value changes.
    
    override func addTarget(_ target: Any?, action: Selector, for controlEvents: UIControl.Event) {
        self.controlEvent = controlEvents
        super.addTarget(target, action: action, for: controlEvents)
    }
    
    // the animation here makes sure the background box slides between the selections.
    
    override func layoutSubviews() {
        positionControls()
        
        UIView.animate(withDuration: 0.25) {
            let button = self.zoomButtons[self.selectedIndex]
            button.setTitleColor(UIColor.black, for: .normal)
            self.selectedBackground.frame = button.frame.insetBy(dx: 0, dy: 0)
        }
        
        super.layoutSubviews()
    }
    
    private func createContent() {
        guard let zoomInfo = zoomInfo else { return }
        
        self.backgroundColor = UIColor.clear
        
        zoomValue = zoomInfo.zoomLevels[selectedIndex]
        
        shadow = UIView(frame: self.bounds)
        shadow.layer.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.35).cgColor
        shadow.layer.cornerRadius = cornerRadius * 2
        addSubview(shadow)
        
        selectedBackground = UIView(frame: CGRect(x: 0, y: 0, width: buttonSize, height: buttonSize))
        selectedBackground.layer.backgroundColor = UIColor.white.cgColor
        selectedBackground.layer.cornerRadius = 10
        addSubview(selectedBackground)
        
        var index = 0
        zoomInfo.zoomLevels.forEach { level in
            let button = UIButton(type: .system)
            button.setTitle(formatZoomValue(level), for: .normal)
            button.setTitleColor(UIColor.yellow, for: .normal)
            button.backgroundColor = UIColor.clear
            button.tag = index
            button.addTarget(self, action: #selector(handlePresetTap(_:)), for: .touchUpInside)
            button.frame = CGRect(x: 0, y: 0, width: buttonSize, height: buttonSize)
            addSubview(button)
            
            zoomButtons.append(button)
            
            index += 1
            maxButtonWidth = max(maxButtonWidth, button.bounds.width)
        }
        
        customZoomLabel = UILabel()
        customZoomLabel.textColor = UIColor.yellow
        customZoomLabel.text = "0x"
        customZoomLabel.textAlignment = .center
        customZoomLabel.sizeToFit()
        customZoomLabel.isHidden = true
        shadow.addSubview(customZoomLabel)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleRotation(_:)), name: UIDevice.orientationDidChangeNotification, object: nil)
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        pan.minimumNumberOfTouches = 1
        pan.maximumNumberOfTouches = 1
        self.addGestureRecognizer(pan)
    }
    
    // MARK: - ZOOM STATUS
    
    // different statuses require different controls to be visible
    
    private func changeStatus(to newStatus: ZoomStatus) {
        switch newStatus {
        case .normal:
            showNormalStatus()
        case .zooming:
            showZoomingStatus()
        case .customZoom:
            showCustomStatus()
        }
        status = newStatus
    }
    
    // the normal status shows all of the presets with one selected. the custom zoom
    // level (used during zooming) is hidden
    private func showNormalStatus() {
        guard let zoomLevels = zoomInfo?.zoomLevels else { return }
        
        // show all the presets and re-label them in case any got changed during zooming
        for i in 0..<zoomLevels.count {
            zoomButtons[i].isHidden = false
            zoomButtons[i].setTitle(formatZoomValue(zoomLevels[i]), for: .normal)
        }
        
        selectedBackground.isHidden = false
        customZoomLabel.isHidden = true
    }
    
    // the zomming status hides all of the presets and slides the shadow area up
    // so the finger can glide back and forth without obscuring the custom zoom
    // value.
    private func showZoomingStatus() {
        guard let zoomLevels = zoomInfo?.zoomLevels else { return }
        
        // hide the presets and re-label them in case any got changed by the
        // custom zoom status
        for i in 0..<zoomLevels.count {
            zoomButtons[i].isHidden = true
            zoomButtons[i].setTitle(formatZoomValue(zoomLevels[i]), for: .normal)
        }
        
        selectedBackground.isHidden = true
        
        UIView.animate(withDuration: 0.15) {
            self.shadow.frame = self.shadow.frame.offsetBy(dx: 0, dy: -60)
        } completion: { _ in
            self.customZoomLabel.isHidden = false
            self.customZoomLabel.text = self.formatZoomValue(self.zoomValue)
        }
    }
    
    // once zooming is complete, the shadow area returns to its normal position and
    // the preset zoomButtons reappear, but one of them has its label changed to the
    // custom zoom value and is selected.
    private func showCustomStatus() {
        replaceButtonLabel(with: zoomValue)
        
        UIView.animate(withDuration: 0.15) {
            self.shadow.frame = self.shadow.frame.offsetBy(dx: 0, dy: 60)
        } completion: { _ in
            self.customZoomLabel.isHidden = true
            self.zoomButtons.forEach { $0.isHidden = false }
            self.selectedBackground.isHidden = false
            self.setNeedsLayout()
        }
    }
    
    // MARK: - LAYOUT and LABELING
    
    // positions the buttons one after the other and then sizes the shadow below
    // the buttons to circumscribe them.
    
    private func positionControls() {
        var xpos: CGFloat = padding
        let ypos: CGFloat = padding
        
        zoomButtons.forEach { button in
            button.frame = CGRect(x: xpos, y: ypos, width: button.bounds.height, height: button.bounds.height)
            button.setTitleColor(UIColor.yellow, for: .normal)
            xpos += spacing+buttonSize
        }
        
        let sframe = CGRect(x: 0, y: 0, width: xpos, height: buttonSize + 2*padding)
        shadow.frame = sframe
        customZoomLabel.frame = shadow.bounds
    }
    
    // replace one of the presets with a new value. which preset is determined by
    // the lowest value between two of the buttons. for example, if there are
    // presets of 1, 2, 4, and 10 and the value is 3.1, then the preset labeled
    // "2" is replaced because 3.1 lies between 2 and 4.
    private func replaceButtonLabel(with newValue: CGFloat) {
        guard let zoomLevels = zoomInfo?.zoomLevels else { return }
        for i in 0..<(zoomButtons.count-1) {
            let minValue = zoomLevels[i]
            let maxValue = zoomLevels[i+1]
            //print("index \(i): \(minValue) <= \(newValue) < \(maxValue)")
            if (minValue..<maxValue).contains(newValue) {
                let rounded = formatZoomValue(newValue)
                zoomButtons[i].setTitle(rounded, for: .normal)
                selectedIndex = i
                return
            }
        }
        
        // default to the last button
        selectedIndex = zoomButtons.count - 1
    }
    
    // returns a string that represents the zoom value as either a whole number (eg, "2x") or
    // with just one decimal place (eg, "2.5x" or "0.5x").
    
    private func formatZoomValue(_ value: CGFloat) -> String {
        guard let zoomInfo = zoomInfo else { return "" }
        
        var result: CGFloat = 0
        
        // we have this weird zoom scale where the ultra wide angle camera is shown as 0.5x but its
        // actual zoom value is 1. The wide angle camera is shown as 1x but its zoom value is
        // 2. The telephoto camera is shown as 2.5 but comes into play with a zoom value of 5.
        switch value {
        case 0...1:
            return "0.5x"
        case 1..<2:
            result = (value - 0.5) * 0.5 + 0.5
        case 2...5:
            result = (value - 2)/3 * 1.5 + 1
        default:
            let base = zoomInfo.zoomMax - 5
            let factor = zoomInfo.zoomMax - 2.5
            result = (value - 5)/base * factor + 2.5
        }
        
        let roundedValue = round(result * 10) / 10.0
        let nearest = roundedValue.rounded(.toNearestOrEven)
        if nearest == roundedValue {
            return String(format: "%.0fx", nearest)
        } else {
            return String(format: "%.1fx", roundedValue)
        }
    }
    
    // MARK: - HANDLERS
    
    // just spin the buttons in place, nother fancy. Since they are squares we don't have
    // to resize the shadow!
    
    @objc private func handleRotation(_ notification: Notification) {
        deviceOrientation = UIDevice.current.orientation.isFlat ? deviceOrientation : UIDevice.current.orientation
        var transform = CGAffineTransform.identity
        
        switch deviceOrientation {
        case .portrait:
            break
        case .landscapeLeft:
            transform = CGAffineTransform(rotationAngle: .pi/2)
        case .landscapeRight:
            transform = CGAffineTransform(rotationAngle: -.pi/2)
        default:
            return
        }
        
        UIView.animate(withDuration: 0.24) {
            self.zoomButtons.forEach { $0.transform = transform }
            self.selectedBackground.transform = transform
            self.customZoomLabel.transform = transform
        }
    }
    
    // handles the tap on a zoom preset value
    
    @objc private func handlePresetTap(_ sender: UIButton) {
        guard isEnabled else { return }
        guard let zoomInfo = zoomInfo else { return }
        
        selectedIndex = sender.tag
        zoomValue = zoomInfo.zoomLevels[selectedIndex]
        changeStatus(to: .normal)
        setNeedsLayout()
        
        if let controlEvent = controlEvent {
            sendActions(for: controlEvent)
        }
    }
    
    // handle the pan gesture to zoom in and out using a single finger
    
    @objc private func handlePan(_ gesture: UIPanGestureRecognizer) {
        guard let zoomMin = zoomInfo?.zoomMin, let zoomMax = zoomInfo?.zoomMax else { return }
        
        switch gesture.state {
        case .began:
            prevZoomHit = zoomValue.rounded(.toNearestOrEven)
            changeStatus(to: .zooming)
            
        case .cancelled, .ended:
            changeStatus(to: .customZoom)
            
        default:
            let velocity = gesture.velocity(in: self)
            let desiredZoomFactor = zoomValue + atan2(velocity.x/200, 5)
            let newZoom = max(zoomMin, min(desiredZoomFactor, zoomMax))
            
            zoomValue = newZoom
            customZoomLabel.text = formatZoomValue(newZoom)
            
            if let controlEvent = controlEvent {
                sendActions(for: controlEvent)
            }
            
            let roundedZoom = newZoom.rounded(.toNearestOrEven)
            if prevZoomHit != roundedZoom {
                generator.selectionChanged()
                prevZoomHit = roundedZoom
            }
        }
    }
    
    // handle the pinch-to-zoom feature as directed here by the camera view controller. this
    // way the same UI can be presented to the user.
    
    public func handlePinchToZoom(gesture: UIPinchGestureRecognizer) {
        guard let zoomMin = zoomInfo?.zoomMin, let zoomMax = zoomInfo?.zoomMax else { return }
        
        switch gesture.state {
        case .began:
            basePinchZoom = zoomValue
            changeStatus(to: .zooming)
            
        case .cancelled, .ended:
            changeStatus(to: .customZoom)
            
        default:
            let newZoom = max(zoomMin, min(basePinchZoom * gesture.scale, zoomMax))
            
            zoomValue = newZoom
            customZoomLabel.text = formatZoomValue(newZoom)
            
            if let controlEvent = controlEvent {
                zoomValue = newZoom
                sendActions(for: controlEvent)
            }
        }
    }
}

