//
//  SKNArrowCircle.swift
//  DrawArc
//
//  Created by PETER ENT on 8/23/21.
//

import UIKit

class SKNArrowCircle: UIView {
    
    var arrowColor: UIColor = .white
    var arrowThickness: CGFloat = 20
    
    private var shapeLayer = CAShapeLayer()
    private let arrowLength: CGFloat = 0.16
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        shapeLayer.strokeColor = arrowColor.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = arrowThickness
        shapeLayer.lineCap = .round
        
        self.layer.addSublayer(shapeLayer)
    }
    
    private func offset(from point: CGPoint, distance: CGFloat, degrees: CGFloat) -> CGPoint {
        let radians = degrees * .pi / 180
        let vertical = sin(radians) * distance
        let horizontal = cos(radians) * distance
        return point.applying(CGAffineTransform(translationX: horizontal, y: vertical))
    }
    
    private func drawUpperArrow(rect: CGRect) -> UIBezierPath {
        let firstPoint = CGPoint(x: rect.size.width * 0.15, y: rect.size.height * 0.25)
        let secondPoint = CGPoint(x: rect.size.width * 0.85, y: rect.size.height * 0.25)
        let controlPoint = CGPoint(x: rect.size.width * 0.5, y: 0)
        
        let arrowEnd1 = offset(from: firstPoint, distance: rect.size.width * arrowLength, degrees: 7)
        let arrowEnd2 = offset(from: firstPoint, distance: rect.size.width * arrowLength, degrees: -87)
        
        let path = UIBezierPath()
        path.move(to: firstPoint)
        path.addQuadCurve(to: secondPoint, controlPoint: controlPoint)
        path.move(to: firstPoint)
        path.addLine(to: arrowEnd1)
        path.move(to: firstPoint)
        path.addLine(to: arrowEnd2)
        return path
    }
    
    private func drawLowerArrow(rect: CGRect) -> UIBezierPath {
        let firstPoint = CGPoint(x: rect.size.width * 0.15, y: rect.size.height * 0.75)
        let secondPoint = CGPoint(x: rect.size.width * 0.85, y: rect.size.height * 0.75)
        let controlPoint = CGPoint(x: rect.size.width * 0.5, y: rect.size.height)
        
        let arrowEnd1 = offset(from: secondPoint, distance: rect.size.width * arrowLength, degrees: 97)
        let arrowEnd2 = offset(from: secondPoint, distance: rect.size.width * arrowLength, degrees: 187)
        
        let path = UIBezierPath()
        path.move(to: firstPoint)
        path.addQuadCurve(to: secondPoint, controlPoint: controlPoint)
        path.move(to: secondPoint)
        path.addLine(to: arrowEnd1)
        path.move(to: secondPoint)
        path.addLine(to: arrowEnd2)
        return path
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        let upperArc = drawUpperArrow(rect: rect)
        let lowerArc = drawLowerArrow(rect: rect)
        let path = UIBezierPath()
        path.append(upperArc)
        path.append(lowerArc)
        shapeLayer.path = path.cgPath
    }
}
