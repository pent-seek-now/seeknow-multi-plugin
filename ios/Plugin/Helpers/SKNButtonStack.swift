//
//  SKNButtonStack.swift
//
//  Created by PETER ENT on 6/15/21.
//

import UIKit
import Combine


class SKNButtonStack: UIView {
    
    private var labels: [String] = []
    private var buttons: [UIButton] = []
    private var subscriptions = Set<AnyCancellable>()
    
    private var aButton: UIButton!
    private var actionTarget: Any?
    private var actionSelector: Selector?
    
    public var selectedIndex = 0
    
    override var intrinsicContentSize: CGSize {
        var minWidth: CGFloat = 0
        var minHeight: CGFloat = 0
        
        buttons.forEach {
            minWidth = max($0.bounds.size.width, minWidth)
            minHeight = max($0.bounds.size.height, minHeight)
        }
        
        return CGSize(width: minWidth, height: minHeight)
    }
    
    var isEnabled: Bool = true {
        didSet {
            self.isUserInteractionEnabled = isEnabled
            self.alpha = isEnabled ? 1.0 : 0.4
        }
    }
    
    init(frame: CGRect, labels: [String]) {
        super.init(frame: frame)
        self.labels = labels
        self.isUserInteractionEnabled = true
        
        labels.publisher
            .map { label -> UIButton in
                let button = UIButton(type: .system)
                button.setTitle(label, for: .normal)
                button.isHidden = true
                button.addTarget(self, action: #selector(handleAction(_:)), for: .touchUpInside)
                return button
            }
            .sink { button in
                self.buttons.append(button)
                self.addSubview(button)
                button.sizeToFit()
            }
            .store(in: &subscriptions)
    }
    
    init(frame: CGRect, buttons: [UIButton]) {
        super.init(frame: frame)
        self.buttons = buttons
        
        buttons.forEach {
            $0.addTarget(self, action: #selector(handleAction(_:)), for: .touchUpInside)
            $0.isHidden = true
            self.addSubview($0)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        buttons.forEach { $0.isHidden = true }
        buttons[selectedIndex].isHidden = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func handleAction(_ sender: Any) {
        selectedIndex += 1
        if selectedIndex >= buttons.count {
            selectedIndex = 0
        }
        setNeedsLayout()
        
        if let target = actionTarget as? NSObject, let selector = actionSelector {
            target.perform(selector, with: nil)
        }
    }
    
    func addTarget(_ observer: Any?, action: Selector) {
        self.actionTarget = observer
        self.actionSelector = action
    }
    
}

class FlashControls: SKNButtonStack {
    
    private let buttonWidth: CGFloat = 40
    private let buttonHeight: CGFloat = 25
    
    init() {
        var result = [UIButton]()
        
        let autoButton = UIButton(type: .system)
        autoButton.frame = CGRect(x: 0, y: 0, width: buttonWidth, height: buttonHeight)
        autoButton.layer.cornerRadius = 8.0
        autoButton.backgroundColor = UIColor.white
        autoButton.setTitle("Auto", for: .normal)
        autoButton.setTitleColor(UIColor.black, for: .normal)
        autoButton.titleLabel?.font = UIFont.systemFont(ofSize: 10)
        result.append(autoButton)
        
        let flashOffButton = UIButton(type: .custom)
        flashOffButton.frame = CGRect(x: 0, y: 0, width: buttonWidth, height: buttonHeight)
        flashOffButton.layer.cornerRadius = 8.0
        flashOffButton.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        flashOffButton.tintColor = UIColor.yellow
        flashOffButton.setImage(UIImage(systemName: "bolt.slash"), for: .normal)
        flashOffButton.contentHorizontalAlignment = .center
        flashOffButton.contentVerticalAlignment = .center
        result.append(flashOffButton)
        
        let flashOnButton = UIButton(type: .custom)
        flashOnButton.frame = CGRect(x: 0, y: 0, width: buttonWidth, height: buttonHeight)
        flashOnButton.layer.cornerRadius = 8.0
        flashOnButton.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        flashOnButton.tintColor = UIColor.yellow
        flashOnButton.contentHorizontalAlignment = .center
        flashOnButton.contentVerticalAlignment = .center
        flashOnButton.setImage(UIImage(systemName: "bolt.fill"), for: .normal)
        result.append(flashOnButton)
        
        super.init(frame: .zero,
                   buttons: result)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
