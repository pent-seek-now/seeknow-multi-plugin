//
//  SKNCameraFocusMeterView.swift
//  PhotoPicker
//
//  Created by PETER ENT on 6/13/21.
//

import UIKit
import CoreMotion

class SKNCameraFocusMeterView: UIView {
    
    private var path: UIBezierPath?
    private var originalTransform: CGAffineTransform?
    
    var isAnimating = false
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.originalTransform = .identity
                
        drawPath(rect)
    }
    
    private func drawPath(_ rect: CGRect) {
        guard path == nil else { return }
        
        let path = UIBezierPath(rect: rect)
        path.move(to: CGPoint(x: rect.width/2, y: 0))
        path.addLine(to: CGPoint(x: rect.width/2, y: 10))
        path.move(to: CGPoint(x: rect.width/2, y: rect.height-10))
        path.addLine(to: CGPoint(x: rect.width/2, y: rect.height))
        path.move(to: CGPoint(x: 0, y: rect.height/2))
        path.addLine(to: CGPoint(x: 10, y: rect.height/2))
        path.move(to: CGPoint(x: rect.width, y: rect.height/2))
        path.addLine(to: CGPoint(x: rect.width-10, y: rect.height/2))
        
        let layer : CAShapeLayer = CAShapeLayer()
        layer.strokeColor = UIColor.systemYellow.cgColor
        layer.lineWidth = 2.0
        layer.fillColor = UIColor.clear.cgColor
        layer.path = path.cgPath
        
        self.layer.addSublayer(layer)
        
        self.path = path
    }
    
    func animate() {
        guard  let originalTransform = originalTransform else { return }
        
        isAnimating = true
        isHidden = false
        
        let scaledTransform = originalTransform.scaledBy(x: 0.25, y: 0.25)
        UIView.animate(withDuration: 0.5) {
            self.transform = scaledTransform
            self.alpha = 0.35
        } completion: { _ in
            self.cancelAnimation()
        }
    }
    
    func cancelAnimation() {
        guard  let originalTransform = originalTransform else { return }
        self.isHidden = true
        self.transform = originalTransform
        self.alpha = 1.0
        self.isAnimating = false
    }
}
