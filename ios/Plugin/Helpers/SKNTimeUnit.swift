//
//  SKNTimeUnit.swift
//  ExamplePhotoApp
//
//  Created by PETER ENT on 8/17/21.
//

import Foundation

public struct SKNTimeUnit {
    
    //
    // Usage: let timeInterval = TimeUnit.hour(5).toTimeInterval()
    //
    
    static public func day(_ value: Int) -> Units {
        return Units.day(value)
    }
    
    static public func hour(_ value: Int) -> Units {
        return Units.hour(value)
    }
    
    static public func minute(_ value: Int) -> Units {
        return Units.minute(value)
    }
    
    static public func second(_ value: Int) -> Units {
        return Units.second(value)
    }
    
    public enum Units {
        case day(Int)
        case hour(Int)
        case minute(Int)
        case second(Int)
        
        public func toTimeInterval() -> TimeInterval {
            switch self {
            case .day(let value):
                return TimeInterval(24 * value * 60 * 60)
            case .hour(let value):
                return TimeInterval(60 * value * 60)
            case .minute(let value):
                return TimeInterval(60 * value)
            case .second(let value):
                return TimeInterval(value)
            }
        }
        
        public func toMilliseconds() -> Int64 {
            return Int64(toTimeInterval()*1000)
        }
        
        // add more conversions like toDays() -> Int or toHours() -> Int
    }
}
