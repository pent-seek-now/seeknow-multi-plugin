//
//  SKNBackgroundService.swift
//  PhotoBrowser
//
//  Created by Peter Ent on 4/12/21.
//

import Foundation
import CoreLocation
import MapKit


/*
 * The SKNBackgroundService monitors the device's geoposition in both the foreground and background. This
 * service also sets up monitors for geofences.
 *
 * Updates are provided via Publishers which can easily be observed.
 *
 * While the app is in the foreground, location changes can come pretty rapidly. The SKNBackgroundService
 * has a `trackingDistanceThreshold` property (value in meters) which is used to set the
 * CLLocationManager's `distanceFilter` to keep the updates down. We could also add a timer
 * to deliver changes every so often.
 *
 * When the app goes into the background, it should call the `startBackgroundTracking` function
 * which changes the CLLocation to use its `startMonitoringForSignificantChanges`.  This
 * is Apple's recommendation. Location updates will now come with greater distance changes (they
 * say 500 meters +/-) and "every 5 minutes or so" depending on the device's battery and apps in use.
 *
 * When the app returns to the foreground, it should call the `startForegroundTracking` function
 * to resume more frequent and accurate updates.
 */
public final class SKNBackgroundService: NSObject, ObservableObject {
    
    static public var shared = SKNBackgroundService()
    
    public enum BackgroundServiceState {
        case uninitialized
        case headingChanged(CLHeading)
        case authorized
        case notAuthorized
    }
    
    public enum LocationTracking {
        case notTracking
        case foreground
        case background
    }
    
    @Published public var trackingType: LocationTracking = .notTracking
    @Published public var state: BackgroundServiceState = .uninitialized
    @Published public var geofenceEvent: SKNGeofenceEvent?
    
    public var currentLocation: CLLocation? = nil
    
    private lazy var locationManager = CLLocationManager()
    private lazy var geofenceKit = SKNGeofenceKit()
    
    private var isBackground = false
    private var apiServer: String?
    private var authToken: String?
    
    private var trackingDistanceThreshold: CLLocationDistance = 100 {
        didSet {
            locationManager.distanceFilter = trackingDistanceThreshold
        }
    }
    
    override private init() {
        super.init()
        
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true /// required
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleAppStateChange(_:)),
                                               name: UIApplication.didEnterBackgroundNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleAppStateChange(_:)),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    public func configure(server: String, authToken: String) {
        self.apiServer = server
        self.authToken = authToken
    }
    
    public func authorizeTracking(isBackground: Bool = false) {
        self.isBackground = isBackground
        if locationManager.authorizationStatus != .authorizedAlways {
            locationManager.requestAlwaysAuthorization()
        } else {
            if isBackground {
                startBackgroundTracking()
            } else {
                startForegroundTracking()
            }
        }
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { _, error in
            if let error = error {
                print(error.localizedDescription)
            }
        }
    }
    
    public func setLocationAccuracy(high: Bool) {
        locationManager.desiredAccuracy = high ? kCLLocationAccuracyBestForNavigation : kCLLocationAccuracyBest
    }
    
    private func startForegroundTracking() {
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.distanceFilter = trackingDistanceThreshold
        locationManager.startUpdatingLocation()
        if CLLocationManager.headingAvailable() {
            locationManager.startUpdatingHeading()
        }
        trackingType = .foreground
    }
    
    private func startBackgroundTracking() {
        locationManager.stopUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        trackingType = .background
    }
    
    @objc private func handleAppStateChange(_ notification: Notification) {
        // use the state change as an oppotunity to remove expired geofences
        SKNGeofenceKit().purgeExpiredGeofences()
    }
}

//
// LOCATION MANAGER DELEGATE
//

extension SKNBackgroundService: CLLocationManagerDelegate {
    
    public func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if manager.authorizationStatus == .authorizedAlways {
            isBackground ? startBackgroundTracking() : startForegroundTracking()
            state = .authorized
        }
        else if manager.authorizationStatus == .authorizedWhenInUse {
            isBackground ? startBackgroundTracking() : startForegroundTracking()
            state = .authorized
        } else {
            state = .notAuthorized
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        self.state = .headingChanged(newHeading)
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let lastLocation = locations.first {
            self.currentLocation = lastLocation
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        guard let geofenceData = geofenceKit.findFence(byId: region.identifier) else { return }
        
        if geofenceKit.isExpired(geofence: geofenceData) {
            geofenceKit.deleteFence(geofenceData)
        } else {
            geofenceEvent = SKNGeofenceEvent(id: region.identifier,
                                                  eventType: .enter,
                                                  timestamp: Date(),
                                                  latitude: geofenceData.latitude,
                                                  longitude: geofenceData.longitude,
                                                  radius: geofenceData.radius,
                                                  data: geofenceData.data)
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        guard let geofenceData = geofenceKit.findFence(byId: region.identifier) else { return }
        
        if geofenceKit.isExpired(geofence: geofenceData) {
            geofenceKit.deleteFence(geofenceData)
        } else {
            geofenceEvent = SKNGeofenceEvent(id: region.identifier,
                                                  eventType: .exit,
                                                  timestamp: Date(),
                                                  latitude: geofenceData.latitude,
                                                  longitude: geofenceData.longitude,
                                                  radius: geofenceData.radius,
                                                  data: geofenceData.data)
        }
    }
}

//
// GEOFENCE
//

public extension SKNBackgroundService {
    
    func addGeofence(center fenceCenter: CLLocationCoordinate2D, identifier id: String, radius fenceRadius: Double) {
        let geofenceRegion = CLCircularRegion(center: fenceCenter, radius: fenceRadius, identifier: id)
        geofenceRegion.notifyOnExit = true
        geofenceRegion.notifyOnEntry = true
        locationManager.startMonitoring(for: geofenceRegion)
    }
    
    func removeGeoFence(center fenceCenter: CLLocationCoordinate2D, identifier id: String, radius fenceRadius: Double) {
        let center = CLLocationCoordinate2D(latitude: fenceCenter.latitude, longitude: fenceCenter.longitude)
        let geofenceRegion = CLCircularRegion(center: center, radius: fenceRadius, identifier: id)
        locationManager.stopMonitoring(for: geofenceRegion)
    }
}
