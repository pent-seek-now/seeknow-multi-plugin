//
//  SKNFileService.swift
//  PhotoBrowser
//
//  Created by Peter Ent on 4/13/21.
//

import UIKit
import CoreLocation
import Combine

/*
 * The FileService provides a way to save an original photo (complete with its
 * metadata), and to load that photo and extract a resized image, gps, and
 * EXIF metadata.
 *
 */
public final class SKNFileService: ObservableObject {
    
    public enum FileServiceError: LocalizedError {
        case createOutputPathFailed
        case photoCreateFailed
        case photoReadFailed
    }

    public enum FileServiceState {
        case uninitialized
        case ready
        case photoSaved(URL)
        case error(FileServiceError)
    }
    
    @Published var state: FileServiceState = .uninitialized
    @Published var photoFilePath: URL? = nil
    
    private lazy var fileManager = FileManager.default
    private var outputURL: URL?
    private var outputPath: String = "" {
        didSet {
            let parts = outputPath.split(separator: "/")
            var result = documentsDirectory
            for part in parts {
                result = result.appendingPathComponent(String(part))
            }
            outputURL = result
        }
    }
    public var documentsDirectory: URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    public init() {
        // empty for public module
    }
    
    public func fileExists(atPath path: String) -> Bool {
        return fileManager.fileExists(atPath: path)
    }
    
    public func createFile(atPath path: String, contents data: Data) {
        fileManager.createFile(atPath: path, contents: data, attributes: nil)
    }
    
    private func createOutputPath(outputPath: String) -> Bool {
        self.outputPath = outputPath
        guard let path = outputURL else { return false }
        
        if !fileManager.fileExists(atPath: path.relativePath) {
            do {
                try fileManager.createDirectory(at: path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print("ERROR: Cannot create output directory: \(outputPath)")
                state = .error(.createOutputPathFailed)
                return false
            }
        }
        
        state = .ready
        return true
    }
    
    
    // MARK: LOAD PHOTO
    
    public func loadPhoto(from filePath: String) -> SKNPhoto? {
        guard let path = URL(string: filePath) else { return nil }
                
        if fileManager.fileExists(atPath: path.relativePath) {
            if let photoData = fileManager.contents(atPath: path.relativePath) {
                return SKNPhoto(originalData: photoData)
            }
        } else {
            print("ERROR: photo not found at path \(filePath)")
            state = .error(.photoReadFailed)
        }
        
        return nil
    }
    
    // MARK: SAVE PHOTO
    
    public func savePhoto(_ photo: SKNPhoto, into outputPath: String, withExtension ext: String) -> Result<String,Error> {
        guard createOutputPath(outputPath: outputPath) else { return .failure(FileServiceError.createOutputPathFailed) }
        
        guard let photoURL = outputURL?
                .appendingPathComponent("photo")//(photo.id)
                .appendingPathExtension(ext) else {
            return .failure(FileServiceError.createOutputPathFailed)
        }

        // file is the original data which contains the metadata. resizing it will lose the metadata
        if !fileManager.createFile(atPath: photoURL.relativePath, contents: photo.originalData, attributes: nil) {
            print("ERROR: Cannot create photo(original) file")
            state = .error(.photoCreateFailed)
            return .failure(FileServiceError.photoCreateFailed)
        }
        
        photoFilePath = photoURL
        state = .photoSaved(photoURL)
        state = .ready
        
        return .success(photoURL.absoluteString)
    }
}
