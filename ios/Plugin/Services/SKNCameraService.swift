//
//  SKNCameraService.swift
//  PhotoBrowser
//
//  Created by Peter Ent on 4/6/21.
//

import UIKit
import Combine
import Photos
import AVFoundation

/*
 * The CameraService provides the interface to the Camera system and is set up to
 * capture a single photo.
 *
 * Users of CameraService should instantiate it and then subscribe to its `state` public
 * publisher for changes. See CameraServiceState for details.
 */
public class SKNCameraService: NSObject {
    
    typealias PhotoCaptureSessionID = String
    
    /*
     * The normal state is "ready" as in ready to take a picture. If the configuring state
     * fails (usually due to lack of authorization), the state becomes error (with a
     * CameraServiceError parameter). Errors taking the picture are usually transitory so
     * the state will return to ready after being error.
     */
    public enum CameraServiceState {
        case uninitialized
        case configuring
        case ready
        case stopped
        case takingPicture
        case processing
        case photoComplete(SKNPhoto)
        case error(CameraServiceError)
    }
    
    /*
     * Errors from the service
     */
    public enum CameraServiceError: LocalizedError {
        case authenticationError
        case configurationError(String)
        case processingError
        case captureError
        case criticalPressure
        
        public var errorDescription: String? {
            switch self {
            case .authenticationError:
                return "Access denined to the camera or too restricted."
            case .configurationError(let message):
                return message
            case .processingError:
                return "Failed to process the image."
            case .captureError:
                return "Failed to capture the image."
            case .criticalPressure:
                return "System pressure reached critical/shutdown level."
            }
        }
    }
    
    /*
     * How the session set up is going/went
     */
    enum SessionSetupResult {
        case success
        case notAuthorized
        case configurationFailed
    }
    
    // MARK: Observed Properties UI must react to
    
    @Published public var state: CameraServiceState = .uninitialized
    
    // MARK: Session Management Properties
    
    public let session = AVCaptureSession()
    var flashMode: AVCaptureDevice.FlashMode = .auto
    var isSessionRunning = false
    var isConfigured = false
    var setupResult: SessionSetupResult = .success
    private var zoomIndex: Int = 0
    
    // Communicate with the session and other session objects on this queue.
    private let sessionQueue = DispatchQueue(label: "CameraService Queue")
    
    @objc dynamic var videoDeviceInput: AVCaptureDeviceInput!
    
    // MARK: Device Configuration Properties
    
    private var cameraDevices: SKNCameraDevices?
    private var systemPressureObserver: NSKeyValueObservation?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Capturing Photos
    
    private let photoOutput = AVCapturePhotoOutput()
    private var inProgressPhotoCaptureDelegates = [Int64: SKNPhotoCaptureProcessor]()
    
    // MARK: Configure
    
    public func configure(devices: SKNCameraDevices) {
        self.cameraDevices = devices
        self.zoomIndex = devices.backZoomInfo?.zoomIndex ?? 0
        /*
         Setup the capture session.
         In general, it's not safe to mutate an AVCaptureSession or any of its
         inputs, outputs, or connections from multiple threads at the same time.
         
         Don't perform these tasks on the main queue because
         AVCaptureSession.startRunning() is a blocking call, which can
         take a long time. Dispatch session setup to the sessionQueue, so
         that the main queue isn't blocked, which keeps the UI responsive.
         */
        sessionQueue.async {
            self.configureSession()
        }
    }
    
    // MARK: Check Permissions
    
    public func checkForPermissions() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            break
        case .notDetermined:
            /*
             The user has not yet been presented with the option to grant
             video access. Suspend the session queue to delay session
             setup until the access request has completed.
             */
            sessionQueue.suspend()
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                if !granted {
                    self.setupResult = .notAuthorized
                    self.state = .error(.authenticationError)
                }
                self.sessionQueue.resume()
            })
            
        default:
            // The user has previously denied access.
            setupResult = .notAuthorized
            state = .error(.authenticationError)
        }
    }
    
    //  MARK: Session Management
    
    private func configureSession() {
        guard !isConfigured else {
            start()
            return
        }
        
        state = .configuring
        session.beginConfiguration()
        session.sessionPreset = .photo
        
        // Add video input.
        do {
            let defaultVideoDevice: AVCaptureDevice? = cameraDevices?.defaultCamera
            
            guard let videoDevice = defaultVideoDevice else {
                setupResult = .configurationFailed
                state = .error(.configurationError("Default video device is unavailable."))
                state = .uninitialized
                session.commitConfiguration()
                return
            }
            
            let videoDeviceInput = try AVCaptureDeviceInput(device: videoDevice)
            
            if session.canAddInput(videoDeviceInput) {
                session.addInput(videoDeviceInput)
                self.videoDeviceInput = videoDeviceInput
                self.videoDeviceInput.device.videoZoomFactor = cameraDevices?.backZoomInfo?.zoomLevels[zoomIndex] ?? 0
                
                NotificationCenter.default.addObserver(self,
                                                       selector: #selector(handleSubjectAreaChange(_:)),
                                                       name: NSNotification.Name.AVCaptureDeviceSubjectAreaDidChange, object: nil)
                
            } else {
                setupResult = .configurationFailed
                state = .error(.configurationError("Could not add a video input device to the session."))
                state = .uninitialized
                session.commitConfiguration()
                return
            }
        } catch {
            setupResult = .configurationFailed
            state = .error(.configurationError("Could not create video device input."))
            state = .uninitialized
            session.commitConfiguration()
            return
        }
        
        // Add the photo output.
        if session.canAddOutput(photoOutput) {
            session.addOutput(photoOutput)
            
            photoOutput.isHighResolutionCaptureEnabled = true
            photoOutput.maxPhotoQualityPrioritization = .balanced
            
        } else {
            setupResult = .configurationFailed
            state = .error(.configurationError("Could not add photo output to the session."))
            state = .uninitialized
            session.commitConfiguration()
            return
        }
        
        systemPressureObserver = observe(\.videoDeviceInput.device.systemPressureState, options: .new) { _, change in
            guard let systemPressureState = change.newValue else { return }
            self.setRecommendedFrameRateRangeForPressureState(systemPressureState: systemPressureState)
        }
        
        session.commitConfiguration()
        
        self.isConfigured = true
        
        self.start()
    }
    
    @objc func handleSubjectAreaChange(_ notification: Notification) {
        ///todo: could use this to remove the focus meter graphic if it hangs around
    }
        
    // MARK: Change Focus
    
    public func focus(at focusPoint: CGPoint){
        let device = self.videoDeviceInput.device
        do {
            try device.lockForConfiguration()
            if device.isFocusPointOfInterestSupported {
                device.focusPointOfInterest = focusPoint
                device.exposurePointOfInterest = focusPoint
                device.exposureMode = .autoExpose//.continuousAutoExposure
                device.focusMode = .autoFocus//.continuousAutoFocus
                device.unlockForConfiguration()
            }
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    public func focus(with focusMode: AVCaptureDevice.FocusMode, exposureMode: AVCaptureDevice.ExposureMode, at devicePoint: CGPoint, monitorSubjectChangeArea: Bool) {
        
        sessionQueue.async {
            let device = self.videoDeviceInput.device
            do {
                try device.lockForConfiguration()
                
                if device.isFocusPointOfInterestSupported && device.isExposureModeSupported(exposureMode) {
                    device.focusPointOfInterest = devicePoint
                    device.focusMode = focusMode
                }
                
                if device.isExposurePointOfInterestSupported && device.isExposureModeSupported(exposureMode) {
                    device.exposurePointOfInterest = devicePoint
                    device.exposureMode = exposureMode
                }
                
                device.isSubjectAreaChangeMonitoringEnabled = monitorSubjectChangeArea
                device.unlockForConfiguration()
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    // MARK: Stop Capture Session
    
    public func stop(completion: (() -> ())? = nil) {
        sessionQueue.async {
            self.systemPressureObserver?.invalidate()
            if self.isSessionRunning {
                if self.setupResult == .success {
                    self.session.stopRunning()
                    self.isSessionRunning = self.session.isRunning
                    
                    if !self.session.isRunning {
                        self.state = .stopped
                        completion?()
                    }
                }
            } else {
                completion?()
            }
        }
    }
    
    // MARK: Start Capture Session
    
    public func start() {
        sessionQueue.async {
            if !self.isSessionRunning && self.isConfigured {
                switch self.setupResult {
                case .success:
                    self.session.startRunning()
                    self.isSessionRunning = self.session.isRunning
                    
                    if self.session.isRunning {
                        self.state = .ready
                    }
                    
                case .configurationFailed, .notAuthorized:
                    self.state = .error(.configurationError("Camera configuration failed. Either your device camera is not available or its missing permissions"))
                    self.state = .uninitialized
                }
            }
        }
    }
    
    // MARK: Set Zoom Level
    
    public func zoom(to zoom: CGFloat, ramp: Bool = true){
        let factor = zoom < 1 ? 1 : zoom
        let device = self.videoDeviceInput.device
        
        do {
            try device.lockForConfiguration()
            device.ramp(toVideoZoomFactor: factor, withRate: 4)
            device.unlockForConfiguration()
        }
        catch {
            print(error.localizedDescription)
            self.state = .error(.configurationError("Error occurred while switch lenses: \(error)"))
        }
        
        self.state = .ready
    }
    
    //    MARK: Capture Photo
    
    public func capturePhoto(orientation: UIInterfaceOrientation, location: CLLocation?, trueHeading: Double?) {
        if self.setupResult != .configurationFailed {
            
            sessionQueue.async {
                
                // flip the image the other way to correct for the orientation of the sensor.
                if let photoOutputConnection = self.photoOutput.connection(with: .video) {
                    switch orientation {
                    case .landscapeLeft:
                        photoOutputConnection.videoOrientation = .landscapeLeft
                    case .landscapeRight:
                        photoOutputConnection.videoOrientation = .landscapeRight
                    default:
                        photoOutputConnection.videoOrientation = .portrait
                    }
                }
                
                let photoSettings: AVCapturePhotoSettings
                
                // Capture hevc photos when supported. Enable according to user settings and high-resolution photos.
                // Otherwise just use JPEG. (it may be a tad faster the capture the image in jpeg)
                if  self.photoOutput.availablePhotoCodecTypes.contains(.hevc) {
                    photoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.hevc])
                } else {
                    photoSettings = AVCapturePhotoSettings()
                }
                                
                // Sets the flash option for this capture.
                if self.videoDeviceInput.device.isFlashAvailable {
                    photoSettings.flashMode = self.flashMode
                }
                
                // Add metadata to capture GPS and heading if present
                var gpsMetadata = [String:Any]()
                
                if let location = location {
                    gpsMetadata[kCGImagePropertyGPSLatitude as String] = fabs(location.coordinate.latitude)
                    gpsMetadata[kCGImagePropertyGPSLatitudeRef as String] = location.coordinate.latitude < 0 ? "S" : "N"
                    gpsMetadata[kCGImagePropertyGPSLongitude as String] = fabs(location.coordinate.longitude)
                    gpsMetadata[kCGImagePropertyGPSLongitudeRef as String] = location.coordinate.longitude < 0 ? "W" : "E"
                    gpsMetadata[kCGImagePropertyGPSAltitude as String] = location.altitude
                    gpsMetadata[kCGImagePropertyGPSAltitudeRef as String] = location.altitude > 0 ? 0 : 1
                    if location.course >= 0 {
                        gpsMetadata[kCGImagePropertyGPSDestBearing as String] = location.course
                        gpsMetadata[kCGImagePropertyGPSDestBearingRef as String] = "T"
                    }
                }
                
                if let trueHeading = trueHeading {
                    gpsMetadata[kCGImagePropertyGPSImgDirection as String] = trueHeading
                    gpsMetadata[kCGImagePropertyGPSImgDirectionRef as String] = "T"
                }
                
                if !gpsMetadata.isEmpty {
                    let metadata = [kCGImagePropertyGPSDictionary as String: gpsMetadata]
                    photoSettings.metadata = metadata
                }
                
                let photoCaptureProcessor = SKNPhotoCaptureProcessor(with: photoSettings, willCapturePhotoAnimation: { [weak self] in
                    // Tells the UI to flash the screen to signal that SwiftCamera took a photo.
                    self?.state = .takingPicture
                    
                }, completionHandler: { [weak self] (photoCaptureProcessor) in
                    // When the capture is complete, remove a reference to the photo capture delegate so it can be deallocated.
                    if let data = photoCaptureProcessor.photoData {
                        self?.state = .photoComplete(SKNPhoto(originalData: data))
                    } else {
                        self?.state = .error(.processingError)
                    }
                    
                    self?.state = .ready
                    self?.inProgressPhotoCaptureDelegates[photoCaptureProcessor.requestedPhotoSettings.uniqueID] = nil
                    
                }, photoProcessingHandler: { [weak self] animate in
                    // Animates a spinner while photo is processing
                    if animate {
                        self?.state = .processing
                    } else {
                        self?.state = .ready
                    }
                })
                
                // The photo output holds a weak reference to the photo capture delegate and stores it in an array to maintain a strong reference.
                self.inProgressPhotoCaptureDelegates[photoCaptureProcessor.requestedPhotoSettings.uniqueID] = photoCaptureProcessor
                self.photoOutput.capturePhoto(with: photoSettings, delegate: photoCaptureProcessor)
            }
        }
    }
    
    // MARK: Handle System Pressure
    
    private func setRecommendedFrameRateRangeForPressureState(systemPressureState: AVCaptureDevice.SystemPressureState) {
        switch systemPressureState.level {
        case .critical, .shutdown:
            print("System pressure reached critical/shutdown levels")
            state = .error(.criticalPressure)
            stop(completion: nil)
        default:
            break
        }
    }
}
