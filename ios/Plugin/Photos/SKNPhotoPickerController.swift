//
//  SKNPhotoPickerController.swift
//  PhotoPlugin
//
//  Created by PETER ENT on 5/14/21.
//

import UIKit
import Photos
import Combine

// MARK: - SKNPhotoPickerDelegate

public protocol SKNPhotoPickerDelegte: AnyObject {
    func libraryPhotoSelected(success: Bool, photo: SKNPhoto?) -> Void
}

// MARK: - SKNPhotoPickerController

public class SKNPhotoPickerController: UIViewController  {
    
    let imageDimension: CGFloat = 80

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        fatalError("Cannot create this via Codable")
    }
    
    public weak var delegate: SKNPhotoPickerDelegte?
    public var options = SKNPhotoPickerOptions()
    
    private var collectionView: UICollectionView!
    private var dismissButton: UIButton!
    
    // This is the result processor, not the actual results. To virtualize
    // this list, we will extract the as neeed.
    private var allPhotosFetchResult: PHFetchResult<PHAsset>?
    
    private lazy var imageManager: PHImageManager = {
        return PHImageManager.default()
    }()
    
    private lazy var imageRequestOptions: PHImageRequestOptions = {
        let options = PHImageRequestOptions()
        options.isSynchronous = true
        options.isNetworkAccessAllowed = true
        return options
    }()
    
    // queue used to process the final photo, based on the options, once selected
    private let processingQueue = DispatchQueue(label: "PhotoPickerProcessor")
    
    // prevent delegate "cancel" being called unnecessarily
    private var needsCancelOnDisappear = true
    
    private func requestAuthorization() {
        PHPhotoLibrary.requestAuthorization(for: .readWrite) { status in
            switch status {
            case .authorized, .limited:
                self.loadPhotos()
            case .denied, .restricted:
                self.showError()
            default:
                // ignore or wait
                break
            }
        }
    }
    
    override public func loadView() {
        super.loadView()
        view.backgroundColor = .white
        
        requestAuthorization()
        
        dismissButton = UIButton()
        dismissButton.setTitle("Cancel", for: .normal)
        dismissButton.setTitleColor(.systemBlue, for: .normal)
        dismissButton.translatesAutoresizingMaskIntoConstraints = false
        dismissButton.addTarget(self, action: #selector(handleCancel(_:)), for: .touchUpInside)
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        layout.minimumLineSpacing = 2
        layout.minimumInteritemSpacing = 2
        layout.estimatedItemSize = CGSize(width: imageDimension, height: imageDimension)
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.allowsSelection = true
        collectionView.register(PhotoCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView.backgroundColor = .white
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(dismissButton)
        view.addSubview(collectionView)
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        dismissButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
        dismissButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        
        collectionView.topAnchor.constraint(equalTo: dismissButton.bottomAnchor, constant: 10).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if needsCancelOnDisappear {
            self.delegate?.libraryPhotoSelected(success: false, photo: nil)
        }
        allPhotosFetchResult = nil
    }
    
    @IBAction func handleCancel(_ sender: Any?) {
        self.delegate?.libraryPhotoSelected(success: false, photo: nil)
        self.needsCancelOnDisappear = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadPhotos() {
        let allPhotosOptions = PHFetchOptions()
        allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        allPhotosFetchResult = PHAsset.fetchAssets(with: .image, options: allPhotosOptions)
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func showError() {
        DispatchQueue.main.async {
            NSLog("PhotoLibrary: %@", "Access Restricted.")
            self.handleCancel(self.dismissButton)
        }
    }
    
    func useSelectionAndDismiss() {
        let selectedIndexes = collectionView.indexPathsForSelectedItems
        let options = PHContentEditingInputRequestOptions()
        options.isNetworkAccessAllowed = true
        
        if let fetcher = allPhotosFetchResult, let selectedIndexes = selectedIndexes {
            
            for index in selectedIndexes {
                let asset = fetcher.object(at: index.item)
                
                asset.requestContentEditingInput(with: options) { editingInput, _ in
                    self.processingQueue.async {
                        var photoResult: SKNPhoto?
                        
                        if let imageURL = editingInput?.fullSizeImageURL {
                            if self.options.createJpegOnly {
                                if let data = try? Data(contentsOf: imageURL) {
                                    photoResult = SKNPhoto(originalData: data).jpegPhoto(size: self.options.targetSize, quality: self.options.jpegQuality)
                                }
                            } else if let data = try? Data(contentsOf: imageURL) {
                                photoResult = SKNPhoto(originalData: data)
                            }
                        }
                        
                        DispatchQueue.main.async {
                            if let photo = photoResult {
                                self.delegate?.libraryPhotoSelected(success: true, photo: photo)
                            } else {
                                self.delegate?.libraryPhotoSelected(success: false, photo: nil)
                            }
                            self.needsCancelOnDisappear = false
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
}

// MARK: - UICollectionView Delegates

extension SKNPhotoPickerController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allPhotosFetchResult?.count ?? 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? PhotoCell
        cell?.backgroundColor = .clear
        
        if let fetcher = self.allPhotosFetchResult, let cell = cell {
            let asset = fetcher.object(at: indexPath.item)
            self.imageManager.requestImage(for: asset,
                                      targetSize: cell.bounds.size,
                                      contentMode: .aspectFill,
                                      options: imageRequestOptions) { image, options in
                if let image = image {
                    cell.image = image
                }
            }
        }
        return cell!
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let layout = collectionViewLayout as? UICollectionViewFlowLayout else { return CGSize.zero }
        
        let numberOfItemsPerRow:CGFloat = 3
        let spacingBetweenCells:CGFloat = layout.minimumInteritemSpacing
        let borderSpacing: CGFloat = layout.sectionInset.left + layout.sectionInset.right
        let totalSpacing = borderSpacing + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        
        var cellSize = CGSize.zero
        
        if let collection = self.collectionView{
            let itemSize = floor((collection.bounds.width - totalSpacing)/numberOfItemsPerRow)
            cellSize = CGSize(width: itemSize, height: itemSize)
        }
        
        return cellSize
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        useSelectionAndDismiss()
    }
}

// MARK: - PhotoCell

extension SKNPhotoPickerController {
    
    class PhotoCell: UICollectionViewCell {
        var imageView: UIImageView!
        var selectionView = UIImageView(image: UIImage(systemName: "checkmark.circle.fill"))
        var selectionBackground = UIImageView(image: UIImage(systemName: "circle.fill"))
        
        var image: UIImage? {
            didSet {
                setNeedsLayout()
            }
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
            imageView.contentMode = .center
            imageView.clipsToBounds = true
            contentView.addSubview(imageView)
            
            selectionBackground.tintColor = UIColor.white
            selectionBackground.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(selectionBackground)
            
            selectionView.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(selectionView)
            
            selectionBackground.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -5).isActive = true
            selectionBackground.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5).isActive = true
            selectionView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -5).isActive = true
            selectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5).isActive = true
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()

            imageView.image = image
            imageView.alpha = isSelected ? 0.45 : 1
            
            selectionView.isHidden = !isSelected
            selectionBackground.isHidden = !isSelected
            
            layer.borderWidth = isSelected ? 2 : 0
            layer.borderColor = UIColor.systemBlue.cgColor
        }
    }
}
