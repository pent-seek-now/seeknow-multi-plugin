//
//  SKNCameraViewModel.swift
//  PhotoBrowser
//
//  Created by Peter Ent on 4/6/21.
//

import Foundation
import Combine
import UIKit
import AVFoundation
import CoreLocation

/*
 * The CameraViewModel pulls together the services: CameraService and LocationService to
 * coordinate their properties. For example, just before the CameraService snaps the photo,
 * it composes an extra GPS metadata to have added to the photo.
 *
 * A number of properties are published so a SwiftUI View can use them to update the UI.
 */

final class SKNCameraViewModel: ObservableObject {
    
    enum ModelState {
        case notReady
        case ready
    }
    
    private let cameraService = SKNCameraService()
    private var locationService: SKNBackgroundService {
        SKNBackgroundService.shared
    }
    
    @Published var state: ModelState = .notReady
    @Published var completePhoto: SKNPhoto!
    @Published var errorMessage: String?
    @Published var locationAvailable = true
    @Published var willCapturePhoto = false
    @Published var disableCameraButton = false
    @Published var isProcessing = false
    @Published var needsConfirmPhoto = false
    @Published var compassHeading: Double = 0
    @Published var flashMode: AVCaptureDevice.FlashMode = .auto
    @Published var photoPath: String?
            
    var session: AVCaptureSession
    var workingPhoto: SKNPhoto!
    
    let cameraDevices = SKNCameraDevices()

    var currentLocation: CLLocation? {
        locationService.currentLocation
    }

    var zoomInfo: SKNCameraDevices.ZoomInfo? {
        cameraDevices.backZoomInfo
    }
    
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        self.session = cameraService.session
        self.state = .ready
        
        // Subscribe to all of the publishers of the CameraService which then, in turn,
        // changes the published properties of this model.
        
        cameraService.$state
            .receive(on: DispatchQueue.main)
            .sink { [weak self] state in
                switch state {
                case .configuring:
                    self?.disableCameraButton = true
                case .takingPicture:
                    self?.willCapturePhoto = true
                    self?.disableCameraButton = true
                case .processing:
                    self?.isProcessing = true
                    self?.disableCameraButton = true
                case .ready:
                    self?.willCapturePhoto = false
                    self?.isProcessing = false
                    self?.disableCameraButton = self?.needsConfirmPhoto ?? false
                case .photoComplete(let photo):
                    self?.workingPhoto = photo
                    self?.needsConfirmPhoto = true
                case .error(let error):
                    self?.errorMessage = error.localizedDescription
                default:
                    break
                }
        }
        .store(in: &self.subscriptions)
        
        locationService.$state
            .receive(on: DispatchQueue.main)
            .sink { [weak self] state in
                switch state {
                case .headingChanged(let heading):
                    self?.compassHeading = heading.trueHeading
                default:
                    break
                }
            }
            .store(in: &self.subscriptions)
    }
    
    func configure() {
        cameraService.checkForPermissions()
        cameraService.configure(devices: cameraDevices)
    }
    
    func endSession(completion: @escaping ()->Void) {
        cameraService.stop() {
            self.completePhoto = nil
            self.workingPhoto = nil
            completion()
        }
        subscriptions.forEach { $0.cancel() }
    }
    
    // MARK: Camera Interfaces
    
    func capturePhoto(orientation: UIInterfaceOrientation) {
        state = .notReady
        cameraService.flashMode = flashMode
        cameraService.capturePhoto(orientation: orientation, location: self.currentLocation, trueHeading: self.compassHeading)
    }

    func zoom(with factor: CGFloat) {
        cameraService.zoom(to: factor)
    }
    
    func retakePhoto() {
        cameraService.state = .ready
        state = .ready
    }
    
    func focus(with focusMode: AVCaptureDevice.FocusMode,
               exposureMode: AVCaptureDevice.ExposureMode,
               at devicePoint: CGPoint,
               monitorSubjectChangeArea: Bool) {
        cameraService.focus(with: focusMode, exposureMode: exposureMode, at: devicePoint, monitorSubjectChangeArea: monitorSubjectChangeArea)
    }
    
    // MARK: - USE THE PHOTO
    
    func usePhoto(options: SKNPhotoPickerOptions) {
        if options.createJpegOnly {
            self.completePhoto = self.workingPhoto.jpegPhoto(size: options.targetSize, quality: options.jpegQuality)
        } else {
            self.completePhoto = self.workingPhoto
        }
        state = .ready
    }
}
