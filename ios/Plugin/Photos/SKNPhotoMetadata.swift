//
//  SKNPhotoMetadata.swift
//  PhotoBrowser
//
//  Created by PETER ENT on 5/7/21.
//

import Foundation
import AVFoundation

// Essentially the EXIF part of the photo's metadata along with
// some other pieces like orientation
//
public struct SKNPhotoMetadata {
    
    public let metadata: [String:Any]
    
    public init(data:[String:Any]?) {
        self.metadata = data ?? [:]
    }
    
    public var asJsonString: String {
        "\(metadata)"
    }
}
