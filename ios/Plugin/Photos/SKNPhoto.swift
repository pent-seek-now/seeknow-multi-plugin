//
//  SKNPhoto.swift
//  PhotoBrowser
//
//  Created by Peter Ent on 4/6/21.
//

import UIKit
import CoreLocation
import AVFoundation

/*
 * The SKNCameraService creates a SKNPhoto which is used by the SKNFileService to save the photo.
 */

public struct SKNPhoto: Identifiable, Hashable, Equatable, Codable {
    
    // The ID of the captured photo
    public var id: String
    
    // Data representation of the captured photo
    public var originalData: Data
    
    public init(id: String = UUID().uuidString, originalData: Data) {
        self.id = id
        self.originalData = originalData
    }
}

public extension SKNPhoto {
    
    var originalImage: UIImage? {
        return UIImage(data: originalData)
    }
    
    var metadata: [String:Any]? {
        if let imageSource = CGImageSourceCreateWithData(originalData as CFData, nil) {
            if let dictionary = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as? [String:Any] {
                return dictionary
            }
        }
        return nil
    }
    
    func thumbnail(width: CGFloat, height: CGFloat, quality: CGFloat = 0.5) -> UIImage? {
        if let data = SKNImageResizer(targetWidth: min(width, height)).resize(data: originalData)?.jpegData(compressionQuality: quality) {
            return UIImage(data: data)
        }
        return nil
    }
}

public extension SKNPhoto {
    
    static func getPreview(from filePath: String) -> UIImage? {
        guard let photo = SKNFileService().loadPhoto(from: filePath) else { return nil }
        return photo.originalImage
    }
    
    static func getResizedImage(from filePath: String, width: CGFloat, height: CGFloat) -> UIImage? {
        guard let photo = SKNFileService().loadPhoto(from: filePath) else { return nil }
        return photo.thumbnail(width: width, height: height)
    }
    
    func jpegPhoto(size: CGSize, quality: CGFloat) -> SKNPhoto? {
        // first step is making JPEG image data, sized and compressed with the given values
        if let jpegData = getJpegData(size: size, quality: quality) {
            let jpegPhoto = SKNPhoto(originalData: jpegData)
            
            // next, extract the metadata from the original (self) photo and the new JPEG data
            if let jpegMetadata = jpegPhoto.metadata, let origMetadata = self.metadata {
                
                // combine the metadata and create a new Photo with the JPEG image data and combined
                // metadata.
                let combinedData = SKNPhoto.combineMetadata(source: origMetadata, target: jpegMetadata)
                if let result = SKNPhoto.blendImageWithMetadata(data: jpegData, metadata: combinedData) {
                    return SKNPhoto(originalData: result)
                }
            }
        }
        return nil
    }
}

// MARK: - MORE PUBLIC APIs

public extension SKNPhoto {
    
    struct GPSData: Codable {
        let latitude: Double
        let longitude: Double
        let altitude: Double
        let direction: Double
    }
    
    static func getPhotoMetadata(from filePath: String) -> SKNPhotoMetadata? {
        guard let photo = SKNFileService().loadPhoto(from: filePath) else { return nil }
        return SKNPhotoMetadata(data: photo.metadata)
    }
    
    static func getGPSData(from filePath: String) -> GPSData? {
        guard let photoMetadata = getPhotoMetadata(from: filePath) else { return nil }
        guard let gpsData = photoMetadata.metadata[kCGImagePropertyGPSDictionary as String] as? [String:Any] else { return nil }

        let latitude: Double = gpsData[kCGImagePropertyGPSLatitude as String] as? Double ?? 0
        let latDir: String = gpsData[kCGImagePropertyGPSLatitudeRef as String] as? String ?? "N"
        let longitude: Double = gpsData[kCGImagePropertyGPSLongitude as String] as? Double ?? 0
        let lonDir: String = gpsData[kCGImagePropertyGPSLongitudeRef as String] as? String ?? "W"
        let altitude: Double = gpsData[kCGImagePropertyGPSAltitude as String] as? Double ?? 0
        let heading: Double = gpsData[kCGImagePropertyGPSImgDirection as String] as? Double ?? 0
        
        let result = GPSData(
            latitude: latitude * (latDir == "N" ? 1.0 : -1.0),
            longitude: longitude * (lonDir == "E" ? 1.0 : -1.0),
            altitude: altitude,
            direction: heading
        )

        return result
    }

}

// MARK: - UTILITY FUNCTIONS

public extension SKNPhoto {
    
    private func getJpegData(size: CGSize, quality: CGFloat) -> Data? {
        guard let image = UIImage(data: originalData) else { return nil }
        if size.width <= 0 || size.height <= 0 {
            return SKNImageResizer().jpegOnly(image: image, quality: quality)
        } else {
            return SKNImageResizer(targetWidth:  min(size.width, size.height)).jpegResize(image: image, quality: quality)
        }
    }
    
    static private func blendImageWithMetadata(data: Data, metadata: [String:Any]) -> Data? {
        let imageSource = CGImageSourceCreateWithData(data as CFData, nil)!
        let uti: CFString = CGImageSourceGetType(imageSource)!
        let dataWithEXIF: NSMutableData = NSMutableData(data: data)
        let destination: CGImageDestination = CGImageDestinationCreateWithData(dataWithEXIF as CFMutableData, uti, 1, nil)!
        
        CGImageDestinationAddImageFromSource(destination, imageSource, 0, metadata as CFDictionary)
        CGImageDestinationFinalize(destination)
        
        return dataWithEXIF as Data
    }
    
    static private func combineMetadata(source: [String:Any], target: [String:Any]) -> [String:Any] {
        var result: [String:Any] = [:]
        
        // first, copy over all of the elements from the original source into the target
        for (key, value) in source {
            if let dictionary = value as? [String:Any] {
                result[key] = combineMetadata(source: dictionary, target: [:])
            } else {
                result[key] = value
            }
        }
        
        // now replace with the corresponding elements from the target. If a target
        // element is a complex element, repeat the process recursively.
        for (key, value) in target {
            if let dictionary = value as? [String:Any], let sourceDict = result[key] as? [String:Any] {
                result[key] = combineMetadata(source: sourceDict, target: dictionary)
            } else {
                result[key] = value
            }
        }
        
        return result
    }
}


