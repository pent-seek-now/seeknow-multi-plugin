//
//  SKNCameraController.swift
//  PhotoPlugin
//
//  Created by PETER ENT on 5/10/21.
//

import UIKit
import AVFoundation
import Combine
import CoreMotion
import MediaPlayer


// MARK: - SKNCameraDelegate

public protocol SKNCameraDelegate: AnyObject {
    func cameraPhotoSelected(success: Bool, photo: SKNPhoto?) -> Void
}

// MARK: - SKNCameraController

public final class SKNCameraController: UIViewController {
    
    private var model = SKNCameraViewModel()
    
    public weak var delegate: SKNCameraDelegate?
    public var cameraOptions = SKNPhotoPickerOptions()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        fatalError("Cannot create this via Codable")
    }
    
    private let previewBackgroundColor = UIColor(displayP3Red: 0.05, green: 0.05, blue: 0.05, alpha: 1.0)
    private let bigButtonSize: CGFloat = 85
    private let medButtonSize: CGFloat = 30
    
    private var topControlArea: UIView!
    private var bottomControlArea: UIView!
    private var previewArea: VideoPreviewView!
    private var shutterButton: UIButton!
    private var closeButton: UIButton!
    private var flashGroup: FlashControls!
    private var zoomControls: SKNZoomControl!
    private var useImageView: UseImagePreview!
    private var apertureView: UIView!
    private var gridView: SKNCameraGrid!
    private var useButton: UIButton!
    private var retakeButton: UIButton!
    private var focusMeteringView: SKNCameraFocusMeterView!
    private var orientationOverlay: UIView!
    private var orientationText: UILabel!
    private var orientationArrows: SKNArrowCircle!
    
    private let audioSession = AVAudioSession.sharedInstance()
    private var subscriptions = Set<AnyCancellable>()
    private var deviceOrientation = UIDeviceOrientation.portrait
    private var currentOrientation = UIInterfaceOrientation.portrait
    private var motionManager: CMMotionManager?
    private var needsLayout = true
    
    // prevent delegate "cancel" being called unnecessarily
    private var needsCancelOnDisappear = true
    
    override public func loadView() {
        super.loadView()
        
        view.backgroundColor = previewBackgroundColor
        
        topControlArea = UIView()
        topControlArea.translatesAutoresizingMaskIntoConstraints = false
        
        bottomControlArea = UIView()
        bottomControlArea.translatesAutoresizingMaskIntoConstraints = false
        
        previewArea = VideoPreviewView()
        previewArea.backgroundColor = .black
        previewArea.videoPreviewLayer.cornerRadius = 0
        previewArea.videoPreviewLayer.videoGravity = .resizeAspectFill
        previewArea.videoPreviewLayer.connection?.videoOrientation = .portrait
        previewArea.videoPreviewLayer.session = model.session
        previewArea.translatesAutoresizingMaskIntoConstraints = false
        previewArea.addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(handlePinchGesture(_:))))
        
        useImageView = UseImagePreview()
        useImageView.isHidden = true
        useImageView.translatesAutoresizingMaskIntoConstraints = false
        
        apertureView = UIView()
        apertureView.backgroundColor = previewBackgroundColor
        apertureView.alpha = 0
        apertureView.translatesAutoresizingMaskIntoConstraints = false
        
        gridView = SKNCameraGrid()
        gridView.isUserInteractionEnabled = false
        gridView.translatesAutoresizingMaskIntoConstraints = false
        
        shutterButton = UIButton()
        shutterButton.setBackgroundImage(UIImage(systemName: "largecircle.fill.circle"), for: .normal)
        shutterButton.tintColor = .white
        shutterButton.isEnabled = false
        shutterButton.translatesAutoresizingMaskIntoConstraints = false
        shutterButton.addTarget(self, action: #selector(handleCapture(_:)), for: .touchUpInside)
        
        flashGroup = FlashControls()
        flashGroup.addTarget(self, action: #selector(handleFlashChange(_:)))
        flashGroup.translatesAutoresizingMaskIntoConstraints = false
        
        closeButton = UIButton()
        closeButton.setTitle("Cancel", for: .normal)
        closeButton.setTitleColor(.white, for: .normal)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.addTarget(self, action: #selector(handleDone(_:)), for: .touchUpInside)
        
        retakeButton = UIButton()
        retakeButton.setTitle("Retake", for: .normal)
        retakeButton.setTitleColor(.systemYellow, for: .normal)
        retakeButton.isHidden = true
        retakeButton.translatesAutoresizingMaskIntoConstraints = false
        retakeButton.addTarget(self, action: #selector(handleRetake(_:)), for: .touchUpInside)
        
        useButton = UIButton()
        useButton.setTitle("Use Photo", for: .normal)
        useButton.setTitleColor(.systemYellow, for: .normal)
        useButton.isHidden = true
        useButton.translatesAutoresizingMaskIntoConstraints = false
        useButton.addTarget(self, action: #selector(handleUse(_:)), for: .touchUpInside)
        
        focusMeteringView = SKNCameraFocusMeterView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        focusMeteringView.isHidden = true
        
        zoomControls = SKNZoomControl(info: model.zoomInfo)
        zoomControls.translatesAutoresizingMaskIntoConstraints = false
        zoomControls.addTarget(self, action: #selector(handleZoomChange(_:)), for: .valueChanged)
        
        orientationOverlay = UIView()
        orientationOverlay.layer.backgroundColor = UIColor.init(white: 0.0, alpha: 0.75).cgColor
        orientationOverlay.isUserInteractionEnabled = false
        orientationOverlay.isHidden = cameraOptions.preferredOrientation == .any
        orientationOverlay.translatesAutoresizingMaskIntoConstraints = false
        
        orientationArrows = SKNArrowCircle()
        orientationArrows.alpha = 0.5
        orientationArrows.translatesAutoresizingMaskIntoConstraints = false
        
        orientationText = UILabel()
        orientationText.text = cameraOptions.preferredOrientation.asLabel
        orientationText.textColor = UIColor.white
        orientationText.textAlignment = .center
        orientationText.backgroundColor = UIColor.clear
        orientationText.font = UIFont.systemFont(ofSize: 24)
        orientationText.sizeToFit()
        orientationText.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(topControlArea)
        view.addSubview(bottomControlArea)
        view.addSubview(previewArea)
        if (cameraOptions.showGrid) { view.addSubview(gridView) }
        view.addSubview(apertureView)
        view.addSubview(useImageView)
        view.addSubview(focusMeteringView)
        view.addSubview(orientationOverlay)
        view.addSubview(zoomControls)
        topControlArea.addSubview(flashGroup)
        bottomControlArea.addSubview(closeButton)
        bottomControlArea.addSubview(shutterButton)
        bottomControlArea.addSubview(retakeButton)
        bottomControlArea.addSubview(useButton)
        orientationOverlay.addSubview(orientationArrows)
        orientationOverlay.addSubview(orientationText)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleRotation(_:)), name: UIDevice.orientationDidChangeNotification, object: nil)
        arrangeOrientationMask()
        
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
                
        // When needsConfirmPhoto has been changed (happens after the user has
        // snapped the photo button and the image is initially processed)
        // the UI lets the user retake or use the photo.
        model.$needsConfirmPhoto
            .dropFirst()
            .sink(receiveValue: { [weak self] value in
                self?.closeButton.isHidden = value
                self?.retakeButton.isHidden = !value
                self?.useButton.isHidden = !value
                self?.useImageView.isHidden = !value
                self?.flashGroup.isEnabled = !value
                self?.shutterButton.isEnabled = !value
                self?.previewArea.isHidden = value
                self?.gridView.isHidden = value
                self?.zoomControls.isHidden = value
                self?.focusMeteringView.isUserInteractionEnabled = !value

                if let photo = self?.model.workingPhoto {
                    self?.useImageView.image = photo.originalImage
                }
            })
            .store(in: &subscriptions)
        
        model.$disableCameraButton
            .sink(receiveValue: { [weak self] value in
                self?.shutterButton.isEnabled = !value
            })
            .store(in: &subscriptions)
        
        model.$isProcessing
            .sink(receiveValue: { [weak self] busy in
                UIView.animate(withDuration: 0.25) {
                    self?.apertureView.alpha = busy ? 1.0 : 0.0
                }
            })
            .store(in: &subscriptions)
        
        model.$completePhoto
            .receive(on: DispatchQueue.main)
            .sink { [weak self] photo in
                if let photo = photo {
                    self?.delegate?.cameraPhotoSelected(success: true, photo: photo)
                    self?.needsCancelOnDisappear = false
                    self?.dismiss(animated: true, completion: nil)
                }
            }
            .store(in: &subscriptions)
        
        model.$errorMessage
            .receive(on: DispatchQueue.main)
            .filter { $0 != nil }
            .sink { [weak self] message in
                NSLog("Camera: %@", message ?? "Unknown Error")
                self?.handleDone(self?.closeButton)
            }
            .store(in: &subscriptions)
        
        try? audioSession.setActive(true)
        audioSession.addObserver(self, forKeyPath: "outputVolume", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.didBecomeActiveNotification, object: nil)
                
        model.configure()
        
        initializeMotionManager()
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        do {
            audioSession.removeObserver(self, forKeyPath: "outputVolume", context: nil)
            try audioSession.setActive(false)
        } catch {
            print("** ISSUE REMOVING AUDIO OBSERVER")
        }
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self)
        
        useImageView.image = nil
        model.endSession() { }
        
        if needsCancelOnDisappear {
            self.delegate?.cameraPhotoSelected(success: false, photo: nil)
        }
    }
    
    override public func updateViewConstraints() {
        super.updateViewConstraints()
        anchorForOrientation()
    }
    
    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @objc func appMovedToBackground() {
        model.endSession {
            // ignored
        }
    }
    
    @objc func appMovedToForeground() {
        model.configure()
    }
    
    private func anchorForOrientation() {
        switch deviceOrientation {
        case .portrait, .portraitUpsideDown, .unknown:
            layoutForPortrait()
        case .landscapeLeft:
            layoutForLandscapeLeft()
        case .landscapeRight:
            layoutForLandscapeRight()
        default:
            // leave as-is if possible but default to portrait
            if needsLayout {
                layoutForPortrait()
            }
            break
        }
    }
    
    private func arrangeOrientationMask() {
        guard cameraOptions.preferredOrientation != .any else { return }
        
        var rotateBy: CGFloat = 0
        var isHidden = false
        
        switch deviceOrientation {
        case .portrait, .portraitUpsideDown, .unknown:
            rotateBy = cameraOptions.preferredOrientation == .landscape ? .pi/2 : 0
            isHidden = cameraOptions.preferredOrientation == .portrait
        case .landscapeLeft:
            rotateBy = cameraOptions.preferredOrientation == .portrait ? 0 : .pi/2
            isHidden = cameraOptions.preferredOrientation == .landscape
        case .landscapeRight:
            rotateBy = cameraOptions.preferredOrientation == .portrait ? 0 : -.pi/2
            isHidden = cameraOptions.preferredOrientation == .landscape
        default:
            break
        }
        
        let transform = CGAffineTransform(rotationAngle: rotateBy)
        UIView.animate(withDuration: 0.25) {
            self.orientationText.transform = transform
            self.orientationOverlay.isHidden = isHidden
        }
    }
    
    public override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        // cannot tell the different between the up and down buttons and this function will not be called
        // if Up is picked and the system volume is already at max nor will it be called if Down is picked
        // and the system volume is already zero.
        if keyPath == "outputVolume" && model.state == .ready {
            handleCapture(self)
        }
    }
    
    @objc func handleRotation(_ notification: Notification) {
        // if the device is flat, leave at last orientatin
        deviceOrientation = UIDevice.current.orientation.isFlat ? deviceOrientation : UIDevice.current.orientation
        arrangeOrientationMask()
        self.view.setNeedsUpdateConstraints()
    }
    
    @IBAction func handleDone(_ sender: Any?) {
        self.delegate?.cameraPhotoSelected(success: false, photo: nil)
        self.needsCancelOnDisappear = false
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func handleCapture(_ sender: Any) {
        shutterButton.isEnabled = false
        model.isProcessing = true
        model.capturePhoto(orientation: currentOrientation)
        model.isProcessing = false
    }
    
    @IBAction func handleRetake(_ sender: Any) {
        model.needsConfirmPhoto = false
        model.retakePhoto()
    }
    
    @IBAction func handleUse(_ sender: Any) {
        model.needsConfirmPhoto = false
        model.usePhoto(options: cameraOptions)
    }
    
    @IBAction func handleFlashChange(_ sender: Any) {
        switch flashGroup.selectedIndex {
        case 0:
            model.flashMode = .auto
        case 1:
            model.flashMode = .off
        default:
            model.flashMode = .on
        }
    }
    
    @IBAction func handleZoomChange(_ sender: Any) {
//        self.model.zoom(index: self.zoomControls.selectedIndex)
        self.model.zoom(with: self.zoomControls.zoomValue)
    }
}

// MARK: - Orientation Layout Handlers

extension SKNCameraController {
    
    func layoutForPortrait(needsRestore: Bool = true) {
        let extraSpace = view.bounds.height - (view.bounds.width * (4.0/3.0))
        let spaceFromTop = max(100.0, extraSpace / 2.0) - 50.0
        let spaceFromBottom = max(50.0, extraSpace / 2.0) + 50.0
        
        topControlArea.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topControlArea.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        topControlArea.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        topControlArea.bottomAnchor.constraint(equalTo: view.topAnchor, constant: spaceFromTop).isActive = true
        
        bottomControlArea.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        bottomControlArea.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        bottomControlArea.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        bottomControlArea.topAnchor.constraint(equalTo: view.bottomAnchor, constant: -spaceFromBottom).isActive = true
        
        previewArea.topAnchor.constraint(equalTo: topControlArea.bottomAnchor, constant: 0).isActive = true
        previewArea.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        previewArea.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        previewArea.bottomAnchor.constraint(equalTo: bottomControlArea.topAnchor).isActive = true
        
        apertureView.topAnchor.constraint(equalTo: topControlArea.bottomAnchor, constant: 0).isActive = true
        apertureView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        apertureView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        apertureView.bottomAnchor.constraint(equalTo: bottomControlArea.topAnchor).isActive = true
        
        if cameraOptions.showGrid {
            gridView.topAnchor.constraint(equalTo: apertureView.topAnchor).isActive = true
            gridView.leadingAnchor.constraint(equalTo: apertureView.leadingAnchor).isActive = true
            gridView.trailingAnchor.constraint(equalTo: apertureView.trailingAnchor).isActive = true
            gridView.bottomAnchor.constraint(equalTo: apertureView.bottomAnchor).isActive = true
        }
        
        useImageView.topAnchor.constraint(equalTo: topControlArea.bottomAnchor, constant: 0).isActive = true
        useImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        useImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        useImageView.bottomAnchor.constraint(equalTo: bottomControlArea.topAnchor).isActive = true
        
        flashGroup.leadingAnchor.constraint(equalTo: topControlArea.leadingAnchor, constant: 10).isActive = true
        flashGroup.centerYAnchor.constraint(equalTo: topControlArea.centerYAnchor).isActive = true
        flashGroup.transform = .identity
        
        zoomControls.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        zoomControls.bottomAnchor.constraint(equalTo: bottomControlArea.topAnchor, constant: -10).isActive = true
        
        shutterButton.centerYAnchor.constraint(equalTo: bottomControlArea.centerYAnchor).isActive = true
        shutterButton.centerXAnchor.constraint(equalTo: bottomControlArea.centerXAnchor).isActive = true
        shutterButton.widthAnchor.constraint(equalToConstant: bigButtonSize).isActive = true
        shutterButton.heightAnchor.constraint(equalToConstant: bigButtonSize).isActive = true
        
        closeButton.leadingAnchor.constraint(equalTo: bottomControlArea.leadingAnchor, constant: 20).isActive = true
        closeButton.centerYAnchor.constraint(equalTo: bottomControlArea.centerYAnchor).isActive = true
        
        retakeButton.centerYAnchor.constraint(equalTo: bottomControlArea.centerYAnchor).isActive = true
        retakeButton.leadingAnchor.constraint(equalTo: bottomControlArea.leadingAnchor, constant: 20).isActive = true
        
        useButton.centerYAnchor.constraint(equalTo: bottomControlArea.centerYAnchor).isActive = true
        useButton.trailingAnchor.constraint(equalTo: bottomControlArea.trailingAnchor, constant: -20).isActive = true
        
        orientationOverlay.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        orientationOverlay.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        orientationOverlay.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        orientationOverlay.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        orientationText.topAnchor.constraint(equalTo: topControlArea.bottomAnchor, constant: 0).isActive = true
        orientationText.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        orientationText.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        orientationText.bottomAnchor.constraint(equalTo: bottomControlArea.topAnchor).isActive = true
        
        orientationArrows.topAnchor.constraint(equalTo: topControlArea.bottomAnchor, constant: 0).isActive = true
        orientationArrows.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        orientationArrows.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        orientationArrows.bottomAnchor.constraint(equalTo: bottomControlArea.topAnchor).isActive = true
        
        if needsRestore {
            UIView.animate(withDuration: 0.25) {
                self.closeButton.transform = .identity
                self.retakeButton.transform = .identity
                self.useButton.transform = .identity
                self.flashGroup.transform = .identity
                self.useImageView.rotate(by: .identity)
            }
        }
    }
    
    func layoutForLandscapeLeft() {
        layoutForPortrait(needsRestore: false)
        let transform = CGAffineTransform(rotationAngle: .pi/2)
        UIView.animate(withDuration: 0.25) {
            self.closeButton.transform = transform
            self.useButton.transform = transform
            self.retakeButton.transform = transform
            self.flashGroup.transform = transform
            self.useImageView.rotate(by: transform)
            
            DispatchQueue.main.async {
                self.useImageView.setNeedsLayout()
            }
        }
    }
    
    func layoutForLandscapeRight() {
        layoutForPortrait(needsRestore: false)
        let transform = CGAffineTransform(rotationAngle: -.pi/2)
        UIView.animate(withDuration: 0.25) {
            self.closeButton.transform = transform
            self.useButton.transform = transform
            self.retakeButton.transform = transform
            self.flashGroup.transform = transform
            self.useImageView.rotate(by: transform)
            
            DispatchQueue.main.async {
                self.useImageView.setNeedsLayout()
            }
        }
    }
}

// MARK: - VideoPreviewView and UseImagePreview

extension SKNCameraController {
    
    // VideoPreviewView shows what the camera sees
    
    class VideoPreviewView: UIView {
        
        override class var layerClass: AnyClass {
            AVCaptureVideoPreviewLayer.self
        }
        
        var videoPreviewLayer: AVCaptureVideoPreviewLayer {
            return layer as! AVCaptureVideoPreviewLayer
        }
    }
    
    // UseImagePreview shows the photo that was just taken
    
    class UseImagePreview: UIView {
        
        var image: UIImage? {
            didSet {
                self.setNeedsDisplay()
            }
        }
        
        private var imageView: UIImageView!
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            imageView = UIImageView(frame: frame)
            addSubview(imageView)
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func rotate(by transform: CGAffineTransform) {
            imageView.transform = transform
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            imageView.frame = self.bounds
            imageView.contentMode = .scaleAspectFit
        }
        
        override func draw(_ rect: CGRect) {
            super.draw(rect)
            if let image = image {
                imageView.image = image
            } else {
                imageView.image = UIImage(systemName: "checkerboard.rectangle")
            }
        }
    }
}

// MARK: - Focus and Pinch-to-Zoom

extension SKNCameraController {
    
    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touchPoint = touches.first else { return }
        guard focusMeteringView.isUserInteractionEnabled else { return }
        
        if focusMeteringView.isAnimating {
            // cancel the animation which means
            // hide this and reset its transaction matrix
            focusMeteringView.cancelAnimation()
        }
        
        let touchPointInPreviewLayer = touchPoint.location(in: previewArea)
        let devicePoint = previewArea.videoPreviewLayer.captureDevicePointConverted(fromLayerPoint: touchPointInPreviewLayer)
        
        // insure that the point is actually in the bounds of the preview
        guard devicePoint.x >= 0 && devicePoint.x <= 1 && devicePoint.y >= 0 && devicePoint.y <= 1 else { return }
        
        model.focus(with: .autoFocus, exposureMode: .autoExpose, at: devicePoint, monitorSubjectChangeArea: true)
        
        let focusPoint = touchPoint.location(in: view)
        focusMeteringView.frame = CGRect(x: focusPoint.x - 50, y: focusPoint.y - 50, width: 100, height: 100)
        focusMeteringView.animate()
    }
    
    @objc public func handlePinchGesture(_ gesture: UIPinchGestureRecognizer) {
        // pass this off to the zoom controls
        zoomControls.handlePinchToZoom(gesture: gesture)
    }
}

// MARK: - Orientation

extension SKNCameraController {
    
    private func initializeMotionManager() {
         motionManager = CMMotionManager()
         motionManager?.accelerometerUpdateInterval = 0.2
         motionManager?.gyroUpdateInterval = 0.2
         motionManager?.startAccelerometerUpdates(to: (OperationQueue.current)!, withHandler: { (accelerometerData, error) -> Void in
            if error == nil {
                self.outputAccelertionData((accelerometerData?.acceleration)!)
            }
            // ignoring errors for this, at least for now
        })
    }
    
    private func outputAccelertionData(_ acceleration: CMAcceleration) {
        var orientationNew: UIInterfaceOrientation
        if acceleration.x >= 0.75 {
            orientationNew = .landscapeLeft
        }
        else if acceleration.x <= -0.75 {
            orientationNew = .landscapeRight
        }
        else if acceleration.y <= -0.75 {
            orientationNew = .portrait

        }
        else if acceleration.y >= 0.75 {
            orientationNew = .portraitUpsideDown
        }
        else {
            // Consider same as last time
            return
        }
        
        if orientationNew == currentOrientation {
            return
        }

        currentOrientation = orientationNew
    }
}
