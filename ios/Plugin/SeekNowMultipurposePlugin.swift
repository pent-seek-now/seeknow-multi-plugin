import Foundation
import Capacitor
import CoreLocation

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */
@objc(SeekNowMultipurposePlugin)
public class SeekNowMultipurposePlugin: CAPPlugin, CLLocationManagerDelegate {
    private let implementation = SeekNowMultipurpose()
    
    private var locationManager: CLLocationManager?
    private var currentLocation: CLLocation?
    private var permissionCallID: String?
        
    @objc func takePhoto(_ call: CAPPluginCall) {
        let pickerOptions = SKNPhotoPickerOptions(createJpegOnly: true,
                                               jpegQuality: 50,
                                               targetSize: CGSize(width: 1280, height: 1280))
//        bridge?.saveCall(call)
        permissionCallID = call.callbackId
        
        DispatchQueue.main.async {
            let cameraView = SKNCameraController()
            cameraView.delegate = self
            cameraView.cameraOptions = pickerOptions
            self.bridge?.viewController?.present(cameraView, animated: true, completion: nil)
        }
    }
    
    @objc func pickPhoto(_ call: CAPPluginCall) {
        let pickerOptions = SKNPhotoPickerOptions(createJpegOnly: true,
                                               jpegQuality: 50,
                                               targetSize: CGSize(width: 1280, height: 1280))
//        bridge?.saveCall(call)
        permissionCallID = call.callbackId
        
        DispatchQueue.main.async {
            let pickerView = SKNPhotoPickerController()
            pickerView.delegate = self
            pickerView.options = pickerOptions
            self.bridge?.viewController?.present(pickerView, animated: true, completion: nil)
        }
    }
    
    func savePhotoAndRespond(success: Bool, photo: SKNPhoto?) {
        if let callID = permissionCallID, let call = bridge!.savedCall(withID: callID) {
            var errorMessage: String?
            
            if success, let photo = photo {
                let fileService = SKNFileService()
                switch fileService.savePhoto(photo, into: "temp/photos", withExtension: "jpg") {
                case .success(let fileUrl):
                    let webUrl = fileUrl //bridge?.portablePath(fromLocalURL: fileUrl) {
                    call.resolve([
                        "path": fileUrl,
                        "webPath": webUrl,
                        "format": "jpeg"
                    ])
                case.failure(let error):
                    errorMessage = error.localizedDescription
                }
            } else {
                errorMessage = "Photo failed to be taken or saved."
            }
            
            if let errorMessage = errorMessage {
                call.reject(errorMessage)
            }
            bridge?.releaseCall(call)
        }
    }
}

extension SeekNowMultipurposePlugin: SKNPhotoPickerDelegte {
    public func libraryPhotoSelected(success: Bool, photo: SKNPhoto?) {
        DispatchQueue.main.async {
            self.bridge?.viewController?.dismiss(animated: true, completion: nil)
        }
        savePhotoAndRespond(success: success, photo: photo)
    }
    
    public func photoSelected(success: Bool, photo: SKNPhoto?) {
        DispatchQueue.main.async {
            self.bridge?.viewController?.dismiss(animated: true, completion: nil)
        }
        savePhotoAndRespond(success: success, photo: photo)
    }
}

extension SeekNowMultipurposePlugin: SKNCameraDelegate {
    public func cameraPhotoSelected(success: Bool, photo: SKNPhoto?) {
        DispatchQueue.main.async {
            self.bridge?.viewController?.dismiss(animated: true, completion: nil)
        }
        savePhotoAndRespond(success: success, photo: photo)
    }
}
