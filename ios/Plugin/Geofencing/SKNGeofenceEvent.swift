//
//  SKNGeofenceEvent.swift
//  MultipurposePlugin
//
//  Created by PETER ENT on 8/19/21.
//

import Foundation

public struct SKNGeofenceEvent {
    
    enum EventType: String {
        case enter = "ENTER"
        case exit = "EXIT"
    }
    
    let id: String
    let eventType: EventType
    let timestamp: Date
    let latitude: Double
    let longitude: Double
    let radius: Double
    let data: String?
}
