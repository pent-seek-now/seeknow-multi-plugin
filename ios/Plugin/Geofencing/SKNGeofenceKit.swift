//
//  SKNGeofenceKit.swift
//  PhotoPicker
//
//  Created by PETER ENT on 8/16/21.
//

import Foundation
import CoreLocation

public class SKNGeofenceKit {
        
    /*
     * Retrieves all of the stored geofences
     */
    public func fetchAllFences() -> [SKNGeofenceData]? {
        let persistenceController = SKNGeofenceRepositoryController.shared
        return persistenceController.fetchAll()
    }
    
    /*
     * locates a specific geofence given its identifier
     */
    public func findFence(byId id: String) -> SKNGeofenceData? {
        let persistenceController = SKNGeofenceRepositoryController.shared
        guard let fence = persistenceController.findGeofences(byId: id)?.first else { return nil }
        return fence
    }
    
    /*
     * creates a new geofence and saves it. This also instructs the background service to
     * start monitoring the device location for crossing events for this new geofence.
     */
    public func saveFence(id: String, coordinate: CLLocationCoordinate2D, radius: Double, duration: Int64, data: String?) {
        let persistenceController = SKNGeofenceRepositoryController.shared
        
        let fence = SKNGeofenceData(data: data ?? "",
                                    duration: duration,
                                    id: id,
                                    latitude: coordinate.latitude,
                                    longitude: coordinate.longitude,
                                    radius: radius,
                                    startDate: Date().timeIntervalSince1970)
        persistenceController.saveGeofence(record: fence)
        
        SKNBackgroundService.shared.addGeofence(center: coordinate, identifier: id, radius: fence.radius)
    }
    
    /*
     * creates a new geofence and saves it. This also instructs the background service to
     * start monitoring the device location for crossing events for this new geofence.
     */
    public func saveFence(id: String, latitude: Double, longitude: Double, radius: Double, duration: Int64, data: String?) {
        saveFence(id: id, coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), radius: radius, duration: duration, data: data)
    }
    
    /*
     * Removes a geofence from storage and instructs the background service to stop monitor for
     * crossing events.
     */
    public func deleteFence(_ geofence: SKNGeofenceData) {
        let coordinate = CLLocationCoordinate2D(latitude: geofence.latitude, longitude: geofence.longitude)
        SKNBackgroundService.shared.removeGeoFence(center: coordinate, identifier: geofence.id, radius: geofence.radius)
        
        let persistenceController = SKNGeofenceRepositoryController.shared
        persistenceController.deleteGeofence(record: geofence)
    }
    
    /*
     * Removes a geofence from storage and instructs the background service to stop monitor for
     * crossing events.
     */
    public func deleteFence(byId id: String) {
        guard let fence = findFence(byId: id) else { return }
        deleteFence(fence)
    }
    
    /*
     * Removes all of the geofences and instructs the background service to stop monitoring them.
     */
    public func deleteAllFences() {
        guard let fences = fetchAllFences() else { return }
        for fence in fences {
            deleteFence(fence)
        }
    }
    
    /*
     * Returns true if the given geofence has expired.
     */
    public func isExpired(geofence: SKNGeofenceData) -> Bool {
        guard geofence.duration > 0 else { return false }
        let now = Date().timeIntervalSince1970
        let duration = TimeInterval(geofence.duration / 1000)
        return now > (geofence.startDate + duration)
    }
    
    /*
     * Goes through the stored geofences and removes all of the ones which have expired.
     */
    public func purgeExpiredGeofences() {
        guard let list = fetchAllFences() else { return }
        list
            .filter( { $0.duration > 0 } )
            .filter( { isExpired(geofence: $0) } )
            .forEach { deleteFence($0) }
    }
}
