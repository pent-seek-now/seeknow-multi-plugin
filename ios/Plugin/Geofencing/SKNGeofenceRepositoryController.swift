//
//  SKNGeofenceRepositoryController.swift
//  PhotoPicker
//
//  Created by PETER ENT on 8/16/21.
//

import Foundation
import CoreData

public struct SKNGeofenceData: Codable {
    var data: String
    var duration: Int64
    var id: String
    var latitude: Double
    var longitude: Double
    var radius: Double
    var startDate: Double
}


final public class SKNGeofenceRepositoryController {
    
    // A singleton for our entire app to use
    public static let shared = SKNGeofenceRepositoryController()

    private let dataFilename = "geofence_data.plist"
    private var cache: [SKNGeofenceData]?
    
    private init() {
        // prevent instance from being created
    }
    
    public func fetchAll() -> [SKNGeofenceData]? {
        let fileService = SKNFileService()
        let documents = fileService.documentsDirectory
        let storageURL = documents.appendingPathComponent(dataFilename)
        
        let decoder = PropertyListDecoder()
        
        guard let data = try? Data.init(contentsOf: storageURL) else {
            cache = nil
            return nil
        }
        
        cache = try? decoder.decode([SKNGeofenceData].self, from: data)
        return cache
    }
    
    public func save() {
        let fileService = SKNFileService()
        let documents = fileService.documentsDirectory
        let storageURL = documents.appendingPathComponent(dataFilename)
        
        let encoder = PropertyListEncoder()
        
        if let data = try? encoder.encode(cache) {
            if fileService.fileExists(atPath: storageURL.path) {
                try? data.write(to: storageURL)
            } else {
                fileService.createFile(atPath: storageURL.path, contents: data)
            }
        }
    }
    
    public func findGeofences(byId id: String) -> [SKNGeofenceData]? {
        return cache?.filter { $0.id == id }
    }
    
    public func saveGeofence(record: SKNGeofenceData) {
        if cache == nil { cache = [SKNGeofenceData]() }
        cache?.append(record)
        save()
    }
    
    public func deleteGeofence(record: SKNGeofenceData) {
        if let index = cache?.firstIndex(where: { $0.id == record.id }) {
            cache?.remove(at: index)
            save()
        }
    }
    
    public func deleteGeofence(byId id: String) {
        if let index = cache?.firstIndex(where: { $0.id == id }) {
            cache?.remove(at: index)
            save()
        }
    }
}
